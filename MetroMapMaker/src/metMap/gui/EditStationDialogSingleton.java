package metMap.gui;

import metMap.data.MetroStation;

import javafx.stage.Stage;
import javafx.stage.Modality;
import javafx.scene.Scene;

import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.FontPosture;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextArea;
import javafx.scene.paint.Color;

/**
 *
 * @author Benjamin Shu
 */
public class EditStationDialogSingleton extends Stage {
    private static EditStationDialogSingleton singleton;
    private static Scene scene;
    private static VBox layout;
    private static Label display;
    private static ColorPicker stationColor;
    private static Color color;
    private static TextArea stationName;
    private static String text;
    private static HBox buttons;
    private static Button confirm, cancel;
    
    private static boolean changed;
    
    private static MetroStation selected;
    
    private static final String EDIT_STATION_TITLE = "Edit Station";
    private static final String DISPLAY_LABEL = "Station Information";
    private static final String CONFIRM_BUTTON = "Confirm Changes";
    private static final String CANCEL_BUTTON = "Cancel";
    
    private static final double LAYOUT_SPACING = 25;
    private static final double STATION_NAME_HEIGHT = 25;
    private static final double STATION_NAME_WIDTH = 150;
    
    private EditStationDialogSingleton() { }
    public static EditStationDialogSingleton getSingleton() {
        if (singleton == null) {
            singleton = new EditStationDialogSingleton();
        }
        return singleton;
    }
    public void init(Stage owner) {
        // Make this window modal, set workspace stage as the owner
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);
        
        // Set up window layout
        layout = new VBox();
        
        // Label window
        display = new Label(DISPLAY_LABEL);
        display.setFont(Font.font("Calibri", FontWeight.BOLD, FontPosture.REGULAR, 20));
        display.setAlignment(Pos.CENTER);
        
        // Set up color picker
        stationColor = new ColorPicker();
        stationColor.setValue(Color.WHITE);
        color = null;
        
        // Set up station name text area
        stationName = new TextArea();
        stationName.setText("");
        stationName.setMaxHeight(STATION_NAME_HEIGHT);
        stationName.setMinHeight(STATION_NAME_HEIGHT);
        stationName.setMaxWidth(STATION_NAME_WIDTH);
        stationName.setMinWidth(STATION_NAME_WIDTH);
        text = "";
        
        selected = null;
        changed = false;
        
        // Set up button row
        buttons = new HBox();
        confirm = new Button(CONFIRM_BUTTON);
        cancel = new Button(CANCEL_BUTTON);
        initButtons();
        buttons.getChildren().addAll(confirm, cancel);
        buttons.setAlignment(Pos.CENTER);
        
        layout.getChildren().addAll(display, stationColor, stationName, buttons);
        layout.setSpacing(LAYOUT_SPACING);
        layout.setAlignment(Pos.CENTER);
        scene = new Scene(layout, 300, 300);
        this.setScene(scene);
        this.setTitle(EDIT_STATION_TITLE);
    }
    private void initButtons() {
        confirm.setOnAction(e -> {
            // Make changes to MetroStation object
            color = stationColor.getValue();
            text = stationName.getText();
            if (color != selected.getColor() || !(text.equals(selected.getName()))) {
                changed = true;
            } else {
                changed = false;
                stationColor.setValue(Color.BLACK);
                color = null;
                stationName.setText("");   
                text = "";             
            }
            this.close();
        });
        cancel.setOnAction(e -> {
            // Clear settings from selected station
            selected = null;
            stationColor.setValue(Color.WHITE);
            stationName.setText("");
            this.close();
        });
    }
    public MetroStation getSelected() {
        return selected;
    }
    public void setSelected(MetroStation nextStation) {
        selected = nextStation;
        stationColor.setValue(nextStation.getColor());
        stationName.setText(nextStation.getName());
    }
    public Color getColor() {
        return color;
    }
    public String getText() {
        return text;
    }
    public boolean changed() {
        return changed;
    }
    public void showDialog() {
        showAndWait();
    }
}
