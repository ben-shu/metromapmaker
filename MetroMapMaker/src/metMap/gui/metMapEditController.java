package metMap.gui;

import djf.AppTemplate;

import metMap.data.metMapData;
import metMap.data.Draggable;
import metMap.data.MetroLine;
import metMap.data.MetroStation;
import metMap.data.MetroImage;
import metMap.data.MetroLabel;

import metMap.data.jTPS;
import metMap.data.Background_Fill;
import metMap.data.Background_Size;
import metMap.data.Node_Add;
import metMap.data.Node_Remove;
import metMap.data.Node_Edit;
import metMap.data.Node_Snap;
import metMap.data.Node_Dragged;
import metMap.data.Node_AdjustWidth;
import metMap.data.Text_Edit;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.shape.Circle;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;

import java.util.ArrayList;

import static metMap.data.metMapState.*;
/**
 * Controller class meant to handle changes made to the map through
 * Metro Map Maker's UI button controls.
 * 
 * @author Benjamin Shu
 */
public class metMapEditController {
    private final AppTemplate app;
    private final metMapData dataManager;
    
    private final ArrayList<MetroLine> lines;
    private final ArrayList<MetroStation> stations;
    private final jTPS transactions;
    
    private final double CANVAS_CENTER_X, CANVAS_CENTER_Y;
    
    private static final String WHITE_HEX = "#FFFFFF";
    private static final String BLACK_HEX = "#000000";
    private static final Color DEFAULT_BACKGROUND_COLOR = Color.valueOf(WHITE_HEX);
    private static final Color DEFAULT_LINE_COLOR = Color.valueOf(BLACK_HEX);
    private static final Color DEFAULT_STATION_FILL = Color.valueOf(WHITE_HEX);
    private static final Color DEFAULT_STATION_STROKE = Color.valueOf(BLACK_HEX);
    
    protected metMapEditController(AppTemplate app) {
        this.app = app;
        this.dataManager = (metMapData) app.getDataComponent();
        this.lines = this.dataManager.getLines();
        this.stations = this.dataManager.getStations();
        this.transactions = this.dataManager.getTransactions();
        this.CANVAS_CENTER_X = this.dataManager.getCanvasWidth() / 2;
        this.CANVAS_CENTER_Y = this.dataManager.getCanvasHeight() / 2;
    }
    public void processAddLine(String lineName) {
        MetroLine line = new MetroLine(lineName, DEFAULT_LINE_COLOR);
        line.setStart(CANVAS_CENTER_X - 50, CANVAS_CENTER_Y);
        line.setEnd(CANVAS_CENTER_X + 50, CANVAS_CENTER_Y);
        line.setStrokeWidth(7.5);
        
        Node_Add transaction = new Node_Add(this.dataManager, line);
        this.transactions.addTransaction(transaction);
        this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
    }
    public void processSelectLine(String lineName) {
        for (MetroLine line : this.lines) {
            if (line.getName().equals(lineName)) {
                this.dataManager.selectShape(line);
                this.dataManager.setState(SELECTED_ELEMENT);
                break;
            }
        }
    }
    public void processEditLine(MetroLine line, Color color, String name, boolean circular) {
        if (line != null) {
            Node_Edit transaction = new Node_Edit(line, color, name, circular);
            this.transactions.addTransaction(transaction);
            this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
        }        
    }
    public void processSetLineCircular(String lineName) {
        MetroLine target = null;
        for (MetroLine line : this.lines) {
            if (lineName.equals(line.getName())) {
                target = line;
                break;
            }
        }
        if (target != null) {
            boolean nextCirc = !target.isCircular();
            Node_Edit transaction = new Node_Edit(target, nextCirc);
            this.transactions.addTransaction(transaction);
            this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
        }
    }
    public void processAdjustLineWidth(MetroLine line, double width) {
        if (line != null) {
            Node_AdjustWidth transaction = new Node_AdjustWidth(line, width);
            this.transactions.addTransaction(transaction);
            this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
        }
    }
    public void processRemoveLine(String lineName) {
        MetroLine target = null;
        for (MetroLine line : this.lines) {
            if (lineName.equals(line.getName())) {
                target = line;
                break;
            }
        }
        if (target != null) {
            Node_Remove transaction = new Node_Remove(this.dataManager, target);
            this.transactions.addTransaction(transaction);
            this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
        }
    }
    public void processAddStationsToLine(String lineName) {
        for (MetroLine line : this.lines) {
            if (lineName.equals(line.getName())) {
                this.dataManager.selectShape(line);
                this.dataManager.setState(ADDING_STATION);
                break;
            }
        }
    }
    public void processRemoveStationsFromLine(String lineName) {
        for (MetroLine line : this.lines) {
            if (lineName.equals(line.getName())) {
                this.dataManager.selectShape(line);
                this.dataManager.setState(REMOVING_STATION);
                break;
            }
        }
    }
    public void processAddStation(String stationName) {
        MetroStation station = new MetroStation(stationName, DEFAULT_STATION_FILL);
        station.setX(this.CANVAS_CENTER_X);
        station.setY(this.CANVAS_CENTER_Y);
        station.setRadius(7.5);
        
        Node_Add transaction = new Node_Add(this.dataManager, station);
        this.transactions.addTransaction(transaction);
        this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
    }
    public void processSelectStation(String stationName) {
        for (MetroStation station : this.stations) {
            if (station.getName().equals(stationName)) {
                this.dataManager.selectShape(station);
                this.dataManager.setState(SELECTED_ELEMENT);
                break;
            }
        }
    }
    public void processEditStation(MetroStation station, Color color, String name) {
        if (station != null) {
            Node_Edit transaction = new Node_Edit(station, color, name);
            this.transactions.addTransaction(transaction);
            this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
        }
    }
    public void processPlaceStationLabel(String stationName) {
        for (MetroStation station : this.stations) {
            if (station.getName().equals(stationName)) {
                Node_Edit transaction = new Node_Edit(station, true, false);
                this.transactions.addTransaction(transaction);
                this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
                break;
            }
        }
    }
    public void processRotateStationLabel(String stationName) {
        for (MetroStation station : this.stations) {
            if (station.getName().equals(stationName)) {
                Node_Edit transaction = new Node_Edit(station, false, true);
                this.transactions.addTransaction(transaction);
                this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
                break;
            }
        }
    }
    public void processAdjustStationRadius(MetroStation station, double radius) {
        if (station != null) {
            Node_AdjustWidth transaction = new Node_AdjustWidth(station, radius);
            this.transactions.addTransaction(transaction);
            this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
        }
    }
    public void processRemoveStation(String stationName) {
        MetroStation target = null;
        for (MetroStation station : this.stations) {
            if (stationName.equals(station.getName())) {
                target = station;
                break;
            }
        }
        if (target != null) {
            Node_Remove transaction = new Node_Remove(this.dataManager, target);
            this.transactions.addTransaction(transaction);
            this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
        }
    }
    public void processAddImage(String imageURI) {
        MetroImage image = new MetroImage(imageURI);
        image.setX(CANVAS_CENTER_X - (image.getLayoutBounds().getWidth() / 2));
        image.setY(CANVAS_CENTER_Y - (image.getLayoutBounds().getHeight() / 2));
        Node_Add transaction = new Node_Add(this.dataManager, image);
        this.transactions.addTransaction(transaction);
        this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
    }
    public void processAddLabel(String text) {
        MetroLabel label = new MetroLabel(text);
        label.setX(CANVAS_CENTER_X - (label.getLayoutBounds().getWidth() / 2));
        label.setY(CANVAS_CENTER_Y - (label.getLayoutBounds().getHeight() / 2));
        Node_Add transaction = new Node_Add(this.dataManager, label);
        this.transactions.addTransaction(transaction);
        this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
    }
    public void processSetBackgroundColor(Color backgroundColor) {
        Background_Fill transaction = new Background_Fill(this.dataManager, backgroundColor);
        this.transactions.addTransaction(transaction);
        this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
    }
    public void processSetBackgroundImage(Image image) {
        ImagePattern backgroundImage = new ImagePattern(image);
        Background_Fill transaction = new Background_Fill(this.dataManager, backgroundImage);
        this.transactions.addTransaction(transaction);
        this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
    }
    public void processToggleBoldItalic(MetroLabel label, boolean bold) {
        if (label != null) {
            Text_Edit transaction = new Text_Edit(label, bold, !bold);
            this.transactions.addTransaction(transaction);
            this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
        }
    }
    public void processEditText(MetroLabel label, Color fontColor) {
        if (label != null) {
            Text_Edit transaction = new Text_Edit(label, fontColor);
            this.transactions.addTransaction(transaction);
            this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
        }
    }
    public void processEditText(MetroLabel label, String fontFamily) {
        if (label != null) {
            Text_Edit transaction = new Text_Edit(label, label.getFontSize(), fontFamily);
            this.transactions.addTransaction(transaction);
            this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
        }
    }
    public void processEditText(MetroLabel label, double fontSize) {
        if (label != null) {
            Text_Edit transaction = new Text_Edit(label, fontSize, label.getFamily());
            this.transactions.addTransaction(transaction);
            this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
        }
    }
    public void processRemoveElement(Shape selected) {
        if (selected != null) {
            Node_Remove transaction = new Node_Remove(this.dataManager, (Draggable) selected);
            this.transactions.addTransaction(transaction);
            this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
        }
    }
    public void processEnlargeMap(double nextHeight, double nextWidth) {
        Background_Size transaction = new Background_Size(this.dataManager,
        ((metMapWorkspace) app.getWorkspaceComponent()).getCanvas(), nextHeight, nextWidth);
        this.transactions.addTransaction(transaction);
        this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
    }
    public void processShrinkMap(double nextHeight, double nextWidth) {
        Background_Size transaction = new Background_Size(this.dataManager,
        ((metMapWorkspace) app.getWorkspaceComponent()).getCanvas(), nextHeight, nextWidth);
        this.transactions.addTransaction(transaction);
        this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
    }
    public void processSnapToGrid(Circle selected) {
        Node_Snap transaction = new Node_Snap(selected);
        this.transactions.addTransaction(transaction);
        this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
    }
    public void processNodeDragged() {
        Node_Dragged transaction = new Node_Dragged();
        this.transactions.addTransaction(transaction);
        this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
    }
}
