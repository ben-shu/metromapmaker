package metMap.gui;

import metMap.data.MetroLine;

import javafx.stage.Stage;
import javafx.stage.Modality;
import javafx.scene.Scene;

import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.CheckBox;
import javafx.scene.paint.Color;
import javafx.scene.control.TextArea;

/**
 *
 * @author Benjamin Shu
 */
public class EditLineDialogSingleton extends Stage {
    private static EditLineDialogSingleton singleton;
    private static Scene scene;
    private static VBox layout;
    private static Label display;
    private static ColorPicker lineColor;
    private static Color color;
    private static TextArea lineName;
    private static String text;
    private static Label checkLabel;
    private static CheckBox circleCheck;
    private static HBox buttons;
    private static Button confirm, cancel;
    
    private static boolean changed;
    
    private static MetroLine selected;
    
    private static final String EDIT_LINE_TITLE = "Edit Line";
    private static final String DISPLAY_LABEL = "Line Information";
    private static final String TOGGLE_CIRCULAR = "Circular?";
    private static final String CONFIRM_BUTTON = "Confirm Changes";
    private static final String CANCEL_BUTTON = "Cancel";
    
    private static final double LAYOUT_SPACING = 25;
    private static final double LINE_NAME_HEIGHT = 25;
    private static final double LINE_NAME_WIDTH = 150;
    
    private EditLineDialogSingleton() { }
    public static EditLineDialogSingleton getSingleton() {
        if (singleton == null) {
            singleton = new EditLineDialogSingleton();
        }
        return singleton;
    }
    public void init(Stage owner) {
        // Make this window modal, set the workspace stage as the owner
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);
        
        // Set up window layout
        layout = new VBox();
        
        // Label window
        display = new Label(DISPLAY_LABEL);
        display.setFont(Font.font("Calibri", FontWeight.BOLD, FontPosture.REGULAR, 20));
        display.setAlignment(Pos.CENTER);
        
        // Set up color picker
        lineColor = new ColorPicker();
        lineColor.setValue(Color.BLACK);
        color = null;
        
        // Set up line name text area
        lineName = new TextArea();
        lineName.setText("");
        lineName.setMaxHeight(LINE_NAME_HEIGHT);
        lineName.setMinHeight(LINE_NAME_HEIGHT);
        lineName.setMaxWidth(LINE_NAME_WIDTH);
        lineName.setMinWidth(LINE_NAME_WIDTH);
        text = "";
        
        checkLabel = new Label(TOGGLE_CIRCULAR);
        checkLabel.setFont(Font.font("Calibri", FontWeight.BOLD, FontPosture.REGULAR, 20));
        checkLabel.setAlignment(Pos.CENTER);
        circleCheck = new CheckBox();
        circleCheck.setAllowIndeterminate(false);
        
        selected = null;
        changed = false;
        
        // Set up button row
        buttons = new HBox();        
        confirm = new Button(CONFIRM_BUTTON);
        cancel = new Button(CANCEL_BUTTON);
        initControllers();
        buttons.getChildren().addAll(confirm, cancel);
        buttons.setAlignment(Pos.CENTER);
        
        layout.getChildren().addAll(display, lineColor, lineName, checkLabel, circleCheck, buttons);
        layout.setSpacing(LAYOUT_SPACING);
        layout.setAlignment(Pos.CENTER);
        scene = new Scene(layout, 300, 300);
        this.setScene(scene);
        this.setTitle(EDIT_LINE_TITLE);
    }
    private void initControllers() {
        confirm.setOnAction(e -> {
            color = lineColor.getValue();
            text = lineName.getText();
            if (color != selected.getColor() || 
                    !(text.equals(selected.getName())) ||
                    !(circleCheck.isSelected() == selected.isCircular())) {
                changed = true;
            }  else {
                changed = false;
                lineColor.setValue(Color.BLACK);
                color = null;
                lineName.setText("");
                text = "";
                circleCheck.setSelected(false);
            }
            this.close();
        });
        cancel.setOnAction(e -> {
            // Clear settings from selected line
            changed = false;
            lineColor.setValue(Color.BLACK);
            color = null;
            lineName.setText("");
            text = "";
            this.close();
        });
    }
    public MetroLine getSelected() {
        return selected;
    }
    public void setSelected(MetroLine nextLine) {
        selected = nextLine;
        lineColor.setValue(nextLine.getColor());
        lineName.setText(nextLine.getName());
        circleCheck.setSelected(nextLine.isCircular());
    }
    public Color getColor() {
        return color;
    }
    public String getText() {
        return text;
    }
    public boolean getCircular() {
        return circleCheck.isSelected();
    }
    public boolean changed() {
        return changed;
    }
    public void showDialog() {
        showAndWait();
    }
}
