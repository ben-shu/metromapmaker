package metMap.gui;

import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import djf.AppTemplate;
import djf.ui.AppGUI;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import djf.ui.AppTextInputDialogSingleton;
import static djf.ui.AppGUI.CLASS_BORDERED_PANE;
import properties_manager.PropertiesManager;

import java.io.File;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

import metMap.data.metMapData;
import metMap.data.jTPS;
import metMap.data.Draggable;
import metMap.data.MetroLine;
import metMap.data.MetroStation;
import metMap.data.MetroImage;
import metMap.data.MetroLabel;
import metMap.file.metMapFiles;

import javafx.scene.layout.Pane;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import javafx.scene.control.CheckBox;

import javafx.scene.Node;
import javafx.scene.shape.Shape;
import javafx.scene.shape.Line;
import javafx.scene.shape.Circle;
import javafx.scene.paint.Color;
import javafx.scene.image.Image;
import javafx.scene.text.Font;

import java.util.HashMap;
import java.util.ArrayList;
import javafx.collections.ObservableList;

import java.io.IOException;

import static metMap.css.metMapStyle.*;
import static djf.settings.AppStartupConstants.*;
import static djf.settings.AppPropertyType.SAVE_COMPLETED_TITLE;
import static djf.settings.AppPropertyType.SAVE_COMPLETED_MESSAGE;
import static djf.settings.AppPropertyType.SAVE_ERROR_TITLE;
import static djf.settings.AppPropertyType.SAVE_ERROR_MESSAGE;
import static metMap.metMapLanguageProperty.*;

import static metMap.data.metMapState.*;
import static metMap.data.metMapData.*;

/**
 * Workspace class built for Metro Map Maker to handle UI controls.
 * 
 * @author Benjamin Shu
 */
public class metMapWorkspace extends AppWorkspaceComponent {
    // The app and GUI that this workspace has been placed in.
    private AppTemplate app;
    private AppGUI gui;
    
    // Application dialog windows
    private AppMessageDialogSingleton messageDialog;
    private AppTextInputDialogSingleton textInput;
    private AppYesNoCancelDialogSingleton yesNoCancel;
    
    // The application's properties manager.
    private PropertiesManager props;
    
    // The metMapData object handling data management.
    private metMapData dataManager;
    // The metMapFiles object handling file saving/loading.
    private metMapFiles fileManager;
    
    // The controller objects for adding, removing, and editing objects.
    private metMapCanvasController canvasControl;
    private metMapEditController editControl;
    
    // The jTPS system used for undo/redo functionality
    private jTPS transactions;
    
    // Toolbar to be added to the AppGUI's top toolbar.
    private FlowPane programButtons;
    
    // Buttons to be added to the AppGUI's file toolbar.
    private Button saveAs, exportMap;
    
    // Buttons for cut/copy/paste and undo/redo functions, to be added to 
    // the AppGUI's top toolbar.
    private Button undo, redo;
    
    // Buttons for opening the Settings and About dialogs for Metro Map Maker.
    // To be added to the AppGUI's file toolbar;
    private Button settings, about;
    
    // VBox meant to hold the UI controls in a neat set of rows
    private VBox editToolbar;
    private static final double EDIT_TOOLBAR_WIDTH = 350;
    private static final double EDIT_SLIDER_WIDTH = 300;
    
    // HBox containing controls for adding, removing, and editing MetroLine 
    // objects.
    private FlowPane lineRow1;
    private Label lineLabel;
    private ComboBox<String> lines;
    private Button editLine;
    private Button addLine, removeLine;
    private Button lineAddStation, lineRemoveStation, lineListStations, lineSetCircular;
    private Slider lineWidth;
    private EditLineDialogSingleton editLineDialog;
    
    // HBox containing controls for adding, removing, and editing MetroStation
    // objects.
    private FlowPane stationRow2;
    private Label stationLabel;
    private ComboBox<String> stations;
    private Button editStation;
    private Button addStation, removeStation;
    private Button snap, placeLabel, rotateLabel;
    private Slider radius;
    private EditStationDialogSingleton editStationDialog;
    
    // HBox containing controls for finding routes between MetroStations.
    private FlowPane routeRow3;
    private ComboBox<String> start, end;
    private Button findRoute;
    private HashMap<MetroStation, ArrayList<Draggable>> map;
    private static final String
            ROUTE_NULL = "There is no available route between those two stations.",
            ORIGIN = "Origin: ", DEST = "Destination: ",
            STOPS = "Total Stops: ", ETA = "Estimated Time: ", MIN = " minutes",
            BOARD = "Board ", TRANSFER = "Transfer to ",
            EXIT = "Exit ", AT = " at ";
    
    // HBox containing controls for adding/removing MetroImages, MetroLabels, 
    // and setting the background color/image fill.
    private FlowPane decorRow4;
    private Label decorLabel;
    private ColorPicker backgroundColor;
    private Button setImageBG;
    private Button addImage, addLabel, removeElement;
    
    // HBox containing controls for editing text elements in Metro Map Maker.
    private FlowPane fontRow5;
    private Label fontLabel;
    private Button bold, italic;
    private ComboBox<Double> fontSize;
    private ComboBox<String> fontFamily;
    private ColorPicker fontColor;
    
    // HBox containing controls for manipulating view of the UI.
    private FlowPane navRow6;
    private Label navLabel, gridLabel;
    private CheckBox showGrid;
    private Button zoomIn, zoomOut, enlargeMap, shrinkMap;
    
    // Pane meant to contain the user's current workspace.
    private Pane canvas;
    private ScrollPane canvasContainer;
    
    // Public constructor designed so that metMapWorkspace can be (and is)
    // instantiated by the app it is placed in
    public metMapWorkspace(AppTemplate initApp) {
        // Keep references to the app that this is placed in and its GUI
        this.app = initApp;
        this.gui = this.app.getGUI();
        this.dataManager = (metMapData) this.app.getDataComponent();
        this.fileManager = (metMapFiles) this.app.getFileComponent();
        this.props = PropertiesManager.getPropertiesManager();
        this.transactions = this.dataManager.getTransactions();
        
        this.messageDialog = AppMessageDialogSingleton.getSingleton();
        this.textInput = AppTextInputDialogSingleton.getSingleton();
        this.yesNoCancel = AppYesNoCancelDialogSingleton.getSingleton();
        
        // Set up panes and controls
        this.initLayout();
        // Add event handlers
        this.initControllers();
        // Apply CSS
        this.initStyle();
    }
    public ComboBox<String> getLineComboBox() {
        return this.lines;
    }
    public void setSelectedLine(String lineName) {
        this.lines.getSelectionModel().select(lineName);
    }
    public Slider getLineWidthSlider() {
        return this.lineWidth;
    }
    public ComboBox<String> getStationComboBox() {
        return this.stations;
    }
    public void setSelectedStation(String stationName) {
        this.stations.getSelectionModel().select(stationName);
    }
    public Slider getStationRadiusSlider() {
        return this.radius;
    }
    public ColorPicker getBackgroundColorPicker() {
        return this.backgroundColor;
    }
    public ComboBox<Double> getFontSizeComboBox() {
        return this.fontSize;
    }
    public ComboBox<String> getFontFamilyComboBox() {
        return this.fontFamily;
    }
    public ColorPicker getFontColorPicker() {
        return this.fontColor;
    }
    public CheckBox getShowGridCheckBox() {
        return this.showGrid;
    }
    public Pane getCanvas() {
        return this.canvas;
    }
    public void initLayout() {
        // Add program-specific buttons to programButtons, then add to top toolbar
        this.programButtons = new FlowPane();
        this.saveAs = gui.initChildButton(this.programButtons, SAVE_AS_ICON.toString(), 
                SAVE_AS_TOOLTIP.toString(), false);
        this.exportMap = gui.initChildButton(this.programButtons, EXPORT_MAP_ICON.toString(), 
                EXPORT_MAP_TOOLTIP.toString(), false);
        this.undo = gui.initChildButton(this.programButtons, UNDO_ICON.toString(), 
                UNDO_TOOLTIP.toString(), true);
        this.redo = gui.initChildButton(this.programButtons, REDO_ICON.toString(), 
                REDO_TOOLTIP.toString(), true);
        this.settings = gui.initChildButton(this.programButtons, SETTINGS_ICON.toString(), 
                SETTINGS_TOOLTIP.toString(), true);
        this.about = gui.initChildButton(this.programButtons, ABOUT_ICON.toString(), 
                ABOUT_TOOLTIP.toString(), false);
        
        // Get reference to AppGUI's top toolbar
        FlowPane topToolbar = gui.getTopToolbarPane();
        topToolbar.getChildren().add(this.programButtons);
        
        // Vertical toolbar for editing controls
        this.editToolbar = new VBox();
        this.editToolbar.setPrefWidth(EDIT_TOOLBAR_WIDTH);
        this.editToolbar.setMaxWidth(EDIT_TOOLBAR_WIDTH);
        this.editToolbar.setMinWidth(EDIT_TOOLBAR_WIDTH);
        
        // Row 1 : MetroLine
        this.lineRow1 = new FlowPane();
        this.lineLabel = new Label(props.getProperty(LINE_LABEL.toString()));
        this.lines = new ComboBox<String>();
        this.lines.setMaxWidth(120); this.lines.setMinWidth(120);
        this.lineRow1.getChildren().addAll(this.lineLabel, this.lines);
        this.editLine = gui.initChildButton(this.lineRow1, EDIT_LINE_ICON.toString(),
                EDIT_LINE_TOOLTIP.toString(), true);
        this.addLine = gui.initChildButton(this.lineRow1, ADD_LINE_ICON.toString(), 
                ADD_LINE_TOOLTIP.toString(), false);
        this.removeLine = gui.initChildButton(this.lineRow1, REMOVE_LINE_ICON.toString(), 
                REMOVE_LINE_TOOLTIP.toString(), true);
        this.lineAddStation = gui.initChildButton(this.lineRow1, LINE_ADD_STATION_ICON.toString(), 
                LINE_ADD_STATION_TOOLTIP.toString(), true);
        this.lineRemoveStation = gui.initChildButton(this.lineRow1, LINE_REMOVE_STATION_ICON.toString(), 
                LINE_REMOVE_STATION_TOOLTIP.toString(), true);
        this.lineListStations = gui.initChildButton(this.lineRow1, LINE_LIST_STATIONS_ICON.toString(), 
                LINE_LIST_STATIONS_TOOLTIP.toString(), true);
        this.lineSetCircular = gui.initChildButton(this.lineRow1, LINE_SET_CIRCULAR_ICON.toString(),
                LINE_SET_CIRCULAR_TOOLTIP.toString(), true);
        this.lineWidth = new Slider();
        this.lineWidth.setBlockIncrement(0.5);
        this.lineWidth.setMin(5.0); this.lineWidth.setMax(10.0);
        this.lineWidth.setDisable(true);
        this.lineWidth.setMaxWidth(EDIT_SLIDER_WIDTH);
        this.lineWidth.setMinWidth(EDIT_SLIDER_WIDTH);
        this.lineRow1.getChildren().add(this.lineWidth);
        
        // Row 2 : MetroStation
        this.stationRow2 = new FlowPane();
        this.stationLabel = new Label(props.getProperty(STATION_LABEL.toString()));
        this.stations = new ComboBox<String>();
        this.stations.setMaxWidth(120); this.stations.setMinWidth(120);
        this.stationRow2.getChildren().addAll(this.stationLabel, this.stations);
        this.editStation = gui.initChildButton(this.stationRow2, EDIT_STATION_ICON.toString(),
                EDIT_STATION_TOOLTIP.toString(), true);
        this.addStation = gui.initChildButton(this.stationRow2, ADD_STATION_ICON.toString(), 
                ADD_STATION_TOOLTIP.toString(), false);
        this.removeStation = gui.initChildButton(this.stationRow2, REMOVE_STATION_ICON.toString(), 
                REMOVE_STATION_TOOLTIP.toString(), true);
        this.snap = gui.initChildButton(this.stationRow2, SNAP_ICON.toString(), 
                SNAP_TOOLTIP.toString(), true);
        this.placeLabel = gui.initChildButton(this.stationRow2, PLACE_LABEL_ICON.toString(), 
                PLACE_LABEL_TOOLTIP.toString(), true);
        this.rotateLabel = gui.initChildButton(this.stationRow2, ROTATE_LABEL_ICON.toString(), 
                ROTATE_LABEL_TOOLTIP.toString(), true);
        this.radius = new Slider();
        this.radius.setBlockIncrement(0.5);
        this.radius.setMin(5.0); this.radius.setMax(10.0);
        this.radius.setDisable(true);
        this.radius.setMaxWidth(EDIT_SLIDER_WIDTH);
        this.radius.setMinWidth(EDIT_SLIDER_WIDTH);
        this.stationRow2.getChildren().add(this.radius);
        
        // Row 3 : Find Routes
        this.routeRow3 = new FlowPane();
        this.start = new ComboBox<String>();
        this.start.setDisable(false);
        this.start.setMinWidth(120); this.start.setMaxWidth(120);
        this.end = new ComboBox<String>();
        this.end.setDisable(false);
        this.end.setMinWidth(120); this.end.setMaxWidth(120);
        this.routeRow3.getChildren().addAll(this.start, this.end);
        this.findRoute = gui.initChildButton(this.routeRow3, FIND_ROUTE_ICON.toString(),
                FIND_ROUTE_TOOLTIP.toString(), false);
        
        // Row 4 : Decor
        this.decorRow4 = new FlowPane();
        this.decorLabel = new Label(props.getProperty(DECOR_LABEL.toString()));
        this.backgroundColor = new ColorPicker();
        this.backgroundColor.setDisable(false);
        this.decorRow4.getChildren().addAll(this.decorLabel, this.backgroundColor);
        this.setImageBG = gui.initChildButton(this.decorRow4, SET_IMAGE_BG_ICON.toString(), 
                SET_IMAGE_BG_TOOLTIP.toString(), false);
        this.addImage = gui.initChildButton(this.decorRow4, ADD_IMAGE_ICON.toString(), 
                ADD_IMAGE_TOOLTIP.toString(), false);
        this.addLabel = gui.initChildButton(this.decorRow4, ADD_LABEL_ICON.toString(), 
                ADD_LABEL_TOOLTIP.toString(), false);
        this.removeElement = gui.initChildButton(this.decorRow4, REMOVE_ELEMENT_ICON.toString(), 
                REMOVE_ELEMENT_TOOLTIP.toString(), true);
        
        // Row 5 : Font Editing
        this.fontRow5 = new FlowPane();
        this.fontLabel = new Label(props.getProperty(FONT_LABEL.toString()));
        this.fontColor = new ColorPicker();
        this.fontColor.setDisable(true);
        this.fontRow5.getChildren().addAll(this.fontLabel, this.fontColor);
        this.bold = gui.initChildButton(this.fontRow5, BOLD_ICON.toString(), 
                BOLD_TOOLTIP.toString(), true);
        this.italic = gui.initChildButton(this.fontRow5, ITALIC_ICON.toString(), 
                ITALIC_TOOLTIP.toString(), true);
        this.fontSize = new ComboBox<Double>();
        this.fontSize.setDisable(true);
        this.fontFamily = new ComboBox<String>();
        this.fontFamily.setDisable(true);
        this.fontRow5.getChildren().addAll(this.fontSize, this.fontFamily);
        
        // Row 6 : Navigation
        this.navRow6 = new FlowPane();
        this.navLabel = new Label(props.getProperty(NAV_LABEL.toString()));
        this.gridLabel = new Label(props.getProperty(GRID_LABEL.toString()));
        this.showGrid = new CheckBox();
        this.showGrid.setDisable(false);
        this.navRow6.getChildren().addAll(this.navLabel, this.gridLabel, this.showGrid);
        this.zoomIn = gui.initChildButton(this.navRow6, ZOOM_IN_ICON.toString(), 
                ZOOM_IN_TOOLTIP.toString(), false);
        this.zoomOut = gui.initChildButton(this.navRow6, ZOOM_OUT_ICON.toString(), 
                ZOOM_OUT_TOOLTIP.toString(), false);
        this.enlargeMap = gui.initChildButton(this.navRow6, ENLARGE_MAP_ICON.toString(), 
                ENLARGE_MAP_TOOLTIP.toString(), false);
        this.shrinkMap = gui.initChildButton(this.navRow6, SHRINK_MAP_ICON.toString(), 
                SHRINK_MAP_TOOLTIP.toString(), false);
        
        // Add all edit rows to editing toolbar
        this.editToolbar.getChildren().addAll(this.lineRow1, this.stationRow2, 
                this.routeRow3, this.decorRow4, this.fontRow5, this.navRow6);
        
        // Set up canvas and sync with data manager.
        this.canvas = new Pane();
        this.canvas.setMaxHeight(600); this.canvas.setMinHeight(600);
        this.canvas.setMaxWidth(1000); this.canvas.setMinWidth(1000);
        this.canvas.setBackground(
                new Background(
                        new BackgroundFill(this.dataManager.getBackground(), null, null)));
        this.dataManager.setElements(this.canvas.getChildren());
        this.canvasContainer = new ScrollPane();
        this.canvasContainer.setContent(this.canvas);
        
        // Set up workspace.
        this.workspace = new HBox();
        this.workspace.getChildren().add(this.editToolbar);
        this.editToolbar.prefHeightProperty().bind(this.workspace.heightProperty());
        this.canvasContainer.minHeightProperty().bind(this.workspace.heightProperty());
        this.canvasContainer.maxHeightProperty().bind(this.workspace.heightProperty()
                .subtract(this.editToolbar.widthProperty()));
        this.canvasContainer.minWidthProperty().bind(this.workspace.widthProperty()
                .subtract(this.editToolbar.widthProperty()));
        this.canvasContainer.maxWidthProperty().bind(this.workspace.widthProperty());;
        this.workspace.getChildren().add(this.canvasContainer);
        
        // Bind editing toolbar row sizes to layout dimensions.
        this.lineRow1.prefWidthProperty().bind(this.editToolbar.widthProperty());
        this.lineRow1.prefHeightProperty().bind(this.editToolbar.heightProperty().divide(6));
        this.stationRow2.prefWidthProperty().bind(this.editToolbar.widthProperty());
        this.stationRow2.prefHeightProperty().bind(this.editToolbar.heightProperty().divide(6));
        this.routeRow3.prefWidthProperty().bind(this.editToolbar.widthProperty());
        this.routeRow3.prefHeightProperty().bind(this.editToolbar.heightProperty().divide(6));
        this.decorRow4.prefWidthProperty().bind(this.editToolbar.widthProperty());
        this.decorRow4.prefHeightProperty().bind(this.editToolbar.heightProperty().divide(6));
        this.fontRow5.prefWidthProperty().bind(this.editToolbar.widthProperty());
        this.fontRow5.prefHeightProperty().bind(this.editToolbar.heightProperty().divide(6));
        this.navRow6.prefWidthProperty().bind(this.editToolbar.widthProperty());
        this.navRow6.prefHeightProperty().bind(this.editToolbar.heightProperty().divide(6));
    }
    public void initControllers() {
        this.editControl = new metMapEditController(this.app);
        this.canvasControl = new metMapCanvasController(this.app);
        this.canvas.setOnMousePressed(e -> {
            this.canvasControl.processMousePress(e.getX(), e.getY());
            
            this.setUndoRedoDisable();
            this.enableEditControls();
        });
        this.canvas.setOnMouseDragged(e -> {
            this.canvasControl.processMouseDragged(e.getX(), e.getY());
            
            this.setUndoRedoDisable();
            this.enableEditControls();
        });
        this.canvas.setOnMouseReleased(e -> {
            this.canvasControl.processMouseReleased(e.getX(), e.getY());
            
            this.setUndoRedoDisable();
            this.enableEditControls();
        });
        this.gui.getPrimaryScene().setOnKeyPressed(e -> {
            double hMax = this.canvasContainer.getHmax(),
                    hMin = this.canvasContainer.getHmin(),
                    vMax = this.canvasContainer.getVmax(),
                    vMin = this.canvasContainer.getVmin(),
                    hVal, vVal;
            switch (e.getCode()) {
                case W :
                    vVal = this.canvasContainer.getVvalue() - (this.canvasContainer.getHeight() / 10);
                    vVal = vVal > vMax ? vMax : vVal;
                    vVal = vVal < vMin ? vMin : vVal;
                    this.canvasContainer.setVvalue(vVal);
                    break;
                case A :
                    hVal = this.canvasContainer.getHvalue() - (this.canvasContainer.getWidth() / 10);
                    hVal = hVal > hMax ? hMax : hVal;
                    hVal = hVal < hMin ? hMin : hVal;
                    this.canvasContainer.setHvalue(hVal);
                    break;
                case S :
                    vVal = this.canvasContainer.getVvalue() + (this.canvasContainer.getHeight() / 10);
                    vVal = vVal > vMax ? vMax : vVal;
                    vVal = vVal < vMin ? vMin : vVal;
                    this.canvasContainer.setVvalue(vVal);
                    break;
                case D :
                    hVal = this.canvasContainer.getHvalue() + (this.canvasContainer.getWidth() / 10);
                    hVal = hVal > hMax ? hMax : hVal;
                    hVal = hVal < hMin ? hMin : hVal;
                    this.canvasContainer.setHvalue(hVal);
                    break;
            }
        });
        // File saving controls
        this.saveAs.setOnAction(e -> {
            this.textInput.showDialog(this.props.getProperty(FILE_NAME_MESSAGE));
            String text = this.textInput.getText();
            if (!text.equals("")) {
                try {
                    this.fileManager.saveData(this.dataManager, PATH_WORK + text);
                    this.messageDialog.show(this.props.getProperty(SAVE_COMPLETED_TITLE),
                            this.props.getProperty(SAVE_COMPLETED_MESSAGE));
                } catch (IOException i) {
                    this.messageDialog.show(this.props.getProperty(SAVE_ERROR_TITLE),
                            this.props.getProperty(SAVE_ERROR_MESSAGE));
                }
            }
        });
        this.exportMap.setOnAction(e -> {
            try {
                this.fileManager.exportData(this.dataManager, 
                        PATH_EXPORT + this.fileManager.getMapName());
                this.messageDialog.show(this.props.getProperty(EXPORT_MAP_TITLE), 
                        this.props.getProperty(EXPORT_MAP_MESSAGE));
            } catch (IOException i) {
                this.messageDialog.show(this.props.getProperty(EXPORT_ERROR_TITLE),
                        this.props.getProperty(EXPORT_ERROR_MESSAGE));
            }
        });
        // Top toolbar editing controls
        this.undo.setOnAction(e -> {
            this.transactions.undoTransaction();
            
            this.reloadWorkspace(this.dataManager);
            
            this.setUndoRedoDisable();
            this.enableEditControls();
        });
        this.redo.setOnAction(e -> {
            this.transactions.doTransaction();
            
            this.reloadWorkspace(this.dataManager);
            
            this.setUndoRedoDisable();
            this.enableEditControls();
        });
        // Settings/About the program
        this.settings.setOnAction(e -> {
            
        });
        this.about.setOnAction(e -> {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(ABOUT_TOOLTIP), 
                    props.getProperty(ABOUT_0) + "\n" +
                            props.getProperty(ABOUT_1) + "\n" +
                            props.getProperty(ABOUT_2) + "\n" +
                            props.getProperty(ABOUT_3));
        });
        // Row 1 : MetroLines
        this.lines.setOnHidden(e -> {
            if (this.lines.getValue() != null) {
                this.editControl.processSelectLine(this.lines.getValue());
                this.stations.getSelectionModel().select(null);
                
                this.enableEditControls();
            }
        });
        this.editLineDialog = EditLineDialogSingleton.getSingleton();
        this.editLineDialog.init(this.gui.getWindow());
        this.editLine.setOnAction(e -> {
            Shape selected = this.dataManager.getSelected();
            if (selected != null && selected instanceof MetroLine) {
                this.editLineDialog.setSelected((MetroLine) selected);
                this.editLineDialog.showDialog();
                if (this.editLineDialog.changed()) {
                    String nextName = this.editLineDialog.getText();
                    for (String lineName : lines.getItems()) {
                        if (lineName.equals(nextName)) {
                            MetroLine check = null;
                            for (MetroLine line : this.dataManager.getLines()) {
                                if (line.getName().equals(lineName)) {
                                    check = line;
                                    break;
                                }
                            }
                            if (check != selected) {
                                this.messageDialog.show(
                                        this.props.getProperty(LINE_NAME_EXISTS_TITLE),
                                        this.props.getProperty(LINE_NAME_EXISTS_MESSAGE));
                                return;
                            }
                        }
                    }
                    this.editControl.processEditLine(this.editLineDialog.getSelected(),
                            this.editLineDialog.getColor(),
                            this.editLineDialog.getText(),
                            this.editLineDialog.getCircular());
                    
                    this.reloadWorkspace(this.dataManager);
                    
                    this.lines.getSelectionModel().select(this.editLineDialog.getText());
                    this.editLine.setBackground(
                            new Background(
                            new BackgroundFill(this.editLineDialog.getColor(), null, null)));
                    
                    this.setUndoRedoDisable();
                }
            }
        });
        this.addLine.setOnAction(e -> {
            this.textInput.showDialog(props.getProperty(LINE_NAME_PROMPT));
            String text = this.textInput.getText();
            if (!(text.equals(""))) {
                for (String line : this.lines.getItems()) {
                    if (text.equals(line)) {
                        this.messageDialog.show(this.props.getProperty(LINE_NAME_EXISTS_TITLE),
                                this.props.getProperty(LINE_NAME_EXISTS_MESSAGE));
                        return;
                    }
                }
                this.editControl.processAddLine(text);
                
                this.reloadWorkspace(this.dataManager);
                
                this.setUndoRedoDisable();
                this.enableEditControls();
            }
        });
        this.removeLine.setOnAction(e -> {
            String lineName = this.lines.getValue();
            this.editControl.processRemoveLine(lineName);
            
            this.reloadWorkspace(this.dataManager);
            
            this.setUndoRedoDisable();
            this.enableEditControls();
        });
        this.lineAddStation.setOnAction(e -> {
            this.editControl.processAddStationsToLine(this.lines.getValue());
            
            this.setUndoRedoDisable();
            this.enableEditControls();
        });
        this.lineRemoveStation.setOnAction(e -> {
            this.editControl.processRemoveStationsFromLine(this.lines.getValue());
            
            this.setUndoRedoDisable();
            this.enableEditControls();
        });
        this.lineListStations.setOnAction(e -> {
            MetroLine selected = (MetroLine) this.dataManager.getSelected();
            String message = "";
            for (MetroStation station : selected.getStations()) {
                message += station.getName() + "\n";
            }
            this.messageDialog.show(this.props.getProperty(LINE_LIST_STATIONS_TITLE), message);
        });
        this.lineSetCircular.setOnAction(e -> {
            this.editControl.processSetLineCircular(this.lines.getValue());
            boolean circular = ((MetroLine) this.dataManager.getSelected()).isCircular();
            
            this.reloadWorkspace(this.dataManager);
            
            this.setUndoRedoDisable();
            this.enableEditControls();
        });
        this.lineWidth.setOnMouseReleased(e -> {
            MetroLine selected = (MetroLine) this.dataManager.getSelected();
            this.editControl.processAdjustLineWidth(selected, this.lineWidth.getValue());
            
            this.reloadWorkspace(this.dataManager);
            
            this.setUndoRedoDisable();
            this.enableEditControls();
        });
        // Row 2 : MetroStations
        this.stations.setOnHidden(e -> {
            if (this.stations.getValue() != null) {
                this.editControl.processSelectStation(this.stations.getValue());
                this.lines.getSelectionModel().select(null);
                
                this.enableEditControls();
            }
        });
        this.editStationDialog = EditStationDialogSingleton.getSingleton();
        this.editStationDialog.init(this.gui.getWindow());
        this.editStation.setOnAction(e -> {
            Shape selected = this.dataManager.getSelected();
            if (selected != null && selected instanceof MetroStation) {
                this.editStationDialog.setSelected((MetroStation) selected);
                this.editStationDialog.showDialog();
                if (this.editStationDialog.changed()) {
                    String nextName = this.editStationDialog.getText();
                    for (String stationName : this.stations.getItems()) {
                        if (stationName.equals(nextName)) {
                            MetroStation check = null;
                            for (MetroStation station : this.dataManager.getStations()) {
                                if (station.getName().equals(stationName)) {
                                    check = station;
                                    break;
                                }
                            }
                            if (check != selected) {
                                this.messageDialog.show(
                                        this.props.getProperty(STATION_NAME_EXISTS_TITLE),
                                        this.props.getProperty(STATION_NAME_EXISTS_MESSAGE));
                                return;
                            }
                        }
                    }
                    this.editControl.processEditStation(this.editStationDialog.getSelected(),
                            this.editStationDialog.getColor(),
                            this.editStationDialog.getText());
                    
                    this.reloadWorkspace(this.dataManager);
                    
                    this.stations.getSelectionModel().select(this.editStationDialog.getText());
                    this.editStation.setBackground(
                            new Background(
                            new BackgroundFill(this.editStationDialog.getColor(), null, null)));
                    
                    this.setUndoRedoDisable();
                }
            }
        });
        this.addStation.setOnAction(e -> {
            this.textInput.showDialog(this.props.getProperty(STATION_NAME_PROMPT));
            String text = this.textInput.getText();
            if (!(text.equals(""))) {
                for (String station : this.stations.getItems()) {
                    if (text.equals(station)) {
                        this.messageDialog.show(this.props.getProperty(STATION_NAME_EXISTS_TITLE),
                                this.props.getProperty(STATION_NAME_EXISTS_MESSAGE));
                        return;
                    }
                }
                this.editControl.processAddStation(text);
                
                this.reloadWorkspace(this.dataManager);
                                
                this.setUndoRedoDisable();
                this.enableEditControls();
            }
        });
        this.removeStation.setOnAction(e -> {
            String stationName = this.stations.getValue();
            this.editControl.processRemoveStation(stationName);
            
            this.reloadWorkspace(this.dataManager);
            
            this.setUndoRedoDisable();
            this.enableEditControls();
        });
        this.snap.setOnAction(e -> {
            Circle selected = (Circle) this.dataManager.getSelected();
            this.editControl.processSnapToGrid(selected);
            
            this.enableEditControls();
            this.setUndoRedoDisable();
        });
        this.placeLabel.setOnAction(e -> {
            this.editControl.processPlaceStationLabel(this.stations.getValue());
            
            this.setUndoRedoDisable();
            this.enableEditControls();
        });
        this.rotateLabel.setOnAction(e -> {
            this.editControl.processRotateStationLabel(this.stations.getValue());
            
            this.setUndoRedoDisable();
            this.enableEditControls();
        });
        this.radius.setOnMouseReleased(e -> {
            MetroStation selected = (MetroStation) this.dataManager.getSelected();
            this.editControl.processAdjustStationRadius(selected, this.radius.getValue());
            
            this.setUndoRedoDisable();
            this.enableEditControls();
        });
        // Row 3 : Route Finding
        this.findRoute.setOnAction(e -> {
            String startName = this.start.getValue(), endName = this.end.getValue();
            this.messageDialog = AppMessageDialogSingleton.getSingleton();
            if (startName != null && endName != null && !startName.equals(endName)) {
                MetroStation src = null, dest = null;
                ArrayList<MetroStation> metStations = this.dataManager.getStations();
                for (MetroStation station : metStations) {
                    if (station.getName().equals(startName)) { src = station; break; }
                }
                for (MetroStation station : metStations) {
                    if (station.getName().equals(endName)) { dest = station; break; }
                }
                if (src != null && dest != null) {
                    String route = "";
                    ArrayList<MetroLine> startPaths = src.getLines();
                    if (startPaths.isEmpty()) {
                        this.messageDialog.show(this.props.getProperty(FIND_ROUTE_TITLE),
                                ROUTE_NULL);
                        return;
                    }
                    ArrayList<MetroLine> endPaths = dest.getLines();
                    if (endPaths.isEmpty()) {
                        this.messageDialog.show(this.props.getProperty(FIND_ROUTE_TITLE),
                                ROUTE_NULL);
                        return;
                    }
                    route += ORIGIN + src.getName() + "\n" + DEST + dest.getName() + "\n";
                    MetroLine link = null;
                    for (MetroLine line : startPaths) {
                        if (endPaths.contains(line)) { link = line; break; }
                    }
                    if (link != null) {
                        String linkName = link.getName();
                        route += linkName;
                        ArrayList<MetroStation> stops = link.getStations();
                        int startIndex = stops.indexOf(src), endIndex = stops.indexOf(dest),
                                num = 0;
                        startIndex = (startIndex < endIndex ? stops.indexOf(src) : stops.indexOf(dest));
                        endIndex = (startIndex < endIndex ? stops.indexOf(dest) : stops.indexOf(src));
                        num = endIndex - startIndex;
                        if (link.isCircular()) {
                            int altNum = startIndex + (stops.size() - endIndex);
                            num = (altNum < num ? altNum : num);
                        }
                        route += " (" + num + " stops)" + "\n"
                                + STOPS + num + "\n"
                                + ETA + (3 * num) + MIN + "\n";
                        route += BOARD + linkName + AT + startName + "\n"
                                + EXIT + linkName + AT + endName + "\n";
                        this.messageDialog.show(this.props.getProperty(FIND_ROUTE_TITLE), route);
                    } else {
                        this.map = new HashMap<>();
                        this.addAllPaths(src, src.getLines(), new ArrayList<>());
                        ArrayList<Draggable> path = this.map.get(dest);
                        if (path != null) {
                            MetroLine currentLine = (MetroLine) path.get(0);
                            int stops = 1, total = 0;
                            String directions = BOARD + currentLine + AT + path.get(1) + "\n";
                            for (int i = 2; i < path.size(); i++) {
                                Draggable point = path.get(i);
                                if (point instanceof MetroLine) {
                                    route += currentLine + " (" + stops + " stops)" + "\n";
                                    currentLine = (MetroLine) point;
                                    total += (stops * 3) + 10;
                                    stops = -1;
                                    directions += TRANSFER + currentLine + AT + path.get(i + 1) + "\n";
                                } else if (point instanceof MetroStation) {
                                    stops++;
                                }
                            }
                            route += currentLine + " (" + stops + " stops)" + "\n" +
                                    ETA + (total + (stops * 3)) + MIN + "\n";
                            directions += EXIT + currentLine + AT + path.get(path.size() - 1);
                            route += directions;
                            this.messageDialog.show(this.props.getProperty(FIND_ROUTE_TITLE), route);
                        } else {
                            this.messageDialog.show(this.props.getProperty(FIND_ROUTE_TITLE),
                                ROUTE_NULL);
                        }
                    }
                }
            }
        });
        // Row 4 : Decor
        this.backgroundColor.setOnAction(e -> {
            this.editControl.processSetBackgroundColor(this.backgroundColor.getValue());
            
            this.setUndoRedoDisable();
            this.enableEditControls();
        });
        this.setImageBG.setOnAction(e -> {
            FileChooser fc = new FileChooser();
            fc.setTitle(this.props.getProperty(IMAGE_FILE_TITLE));
            fc.getExtensionFilters().add(new ExtensionFilter("Image Files", "*.png", "*.jpg", "*.pdf"));
            File imageFile = fc.showOpenDialog(this.app.getGUI().getWindow());
            if (imageFile != null) {
                String imageURI = imageFile.toURI().toString();
                Image backgroundImage = new Image(imageURI);
                this.dataManager.setBackgroundURI(imageURI);
                this.editControl.processSetBackgroundImage(backgroundImage);
                this.setUndoRedoDisable();
            }
        });
        this.addImage.setOnAction(e -> {
            FileChooser fc = new FileChooser();
            fc.setTitle(this.props.getProperty(IMAGE_FILE_TITLE));
            fc.getExtensionFilters().add(new ExtensionFilter("Image Files", "*.png", "*.jpg", "*.pdf"));
            File imageFile = fc.showOpenDialog(this.app.getGUI().getWindow());
            if (imageFile != null) {
                String imageURI = imageFile.toURI().toString();
                this.editControl.processAddImage(imageURI);
                
                this.setUndoRedoDisable();
                this.enableEditControls();
            }
        });
        this.addLabel.setOnAction(e -> {
            this.textInput.showDialog(props.getProperty(LABEL_TEXT_PROMPT));
            String text = textInput.getText();
            if (!text.equals("")) {
                this.editControl.processAddLabel(text);
                
                this.setUndoRedoDisable();
                this.enableEditControls();
            }
        });
        this.removeElement.setOnAction(e -> {
            Shape selected = this.dataManager.getSelected();
            if (selected != null) {
                if (selected instanceof Draggable) {
                    if (selected instanceof MetroLine) {
                        String lineName = this.lines.getValue();
                        this.editControl.processRemoveLine(lineName);
                        this.lines.getItems().remove(lineName);
                        this.lines.getSelectionModel().select(null);
                    } else if (selected instanceof MetroStation) {
                        String stationName = this.stations.getValue();
                        this.editControl.processRemoveStation(stationName);
                        this.stations.getItems().remove(stationName);
                        this.stations.getSelectionModel().select(null);
                    } else if (selected instanceof MetroImage) {
                        this.editControl.processRemoveElement(selected);
                    } else if (selected instanceof MetroLabel) {
                        this.editControl.processRemoveElement(selected);
                    }
                    this.setUndoRedoDisable();
                    this.enableEditControls();
                }
            }
            this.removeElement.setDisable(true);
        });
        // Row 5 : Text Editing
        this.fontColor.setValue(Color.BLACK);
        this.fontColor.setOnAction(e -> {
            MetroLabel selected = (MetroLabel) this.dataManager.getSelected();
            this.editControl.processEditText(selected, this.fontColor.getValue());
            
            this.setUndoRedoDisable();
            this.enableEditControls();
        });
        this.bold.setOnAction(e -> {
            MetroLabel selected = (MetroLabel) this.dataManager.getSelected();
            this.editControl.processToggleBoldItalic(selected, true);
            
            this.setUndoRedoDisable();
            this.enableEditControls();
        });
        this.italic.setOnAction(e -> {
            MetroLabel selected = (MetroLabel) this.dataManager.getSelected();
            this.editControl.processToggleBoldItalic(selected, false);
            
            this.setUndoRedoDisable();
            this.enableEditControls();
        });
        this.fontSize.setVisibleRowCount(5);
        for (int i = 12; i <= 36; i++) {
            this.fontSize.getItems().add((double) i);
        }
        this.fontSize.setOnHidden(e -> {
            if (this.fontSize.getValue() != null) {
                MetroLabel selected = (MetroLabel) this.dataManager.getSelected();
                this.editControl.processEditText(selected, this.fontSize.getValue());
                
                this.setUndoRedoDisable();
                this.enableEditControls();
            }
        });
        this.fontFamily.setMaxWidth(100);
        this.fontFamily.setMinWidth(100);
        this.fontFamily.setVisibleRowCount(5);
        for (String fontName : Font.getFamilies()) {
            this.fontFamily.getItems().add(fontName);
        }
        this.fontFamily.setOnHidden(e -> {
            if (this.fontFamily.getValue() != null) {
                MetroLabel selected = (MetroLabel) this.dataManager.getSelected();
                this.editControl.processEditText(selected, this.fontFamily.getValue());
                
                this.setUndoRedoDisable();
                this.enableEditControls();
            }
        });
        // Row 6 : Workspace Navigation
        this.showGrid.setOnAction(e -> {
            if (this.showGrid.isSelected()) {
                ObservableList<Node> nodes = this.canvas.getChildren();
                Line line;
                double height = this.canvas.getHeight(), width = this.canvas.getWidth();
                for (int i = 0; i < width; i += 24) {
                    line = new Line();
                    line.setStartX(i); line.setStartY(0);
                    line.setEndX(i); line.setEndY(height);
                    line.setStrokeWidth(1);
                    nodes.add(0, line);
                }
                for (int i = 0; i < height; i += 24) {
                    line = new Line();
                    line.setStartX(0); line.setStartY(i);
                    line.setEndX(width); line.setEndY(i);
                    line.setStrokeWidth(1);
                    nodes.add(0, line);
                }
            } else {
                ObservableList<Node> nodes = this.canvas.getChildren();
                Node node;
                do {
                    node = nodes.get(0);
                    if (node instanceof Line) {
                        nodes.remove(0);
                    }
                } while (node instanceof Line);
            }
        });
        this.zoomIn.setOnAction(e -> {
            this.canvas.setScaleX(this.canvas.getScaleX() * 1.1);
            this.canvas.setScaleY(this.canvas.getScaleY() * 1.1);
        });
        this.zoomOut.setOnAction(e -> {
            this.canvas.setScaleX(this.canvas.getScaleX() / 1.1);
            this.canvas.setScaleY(this.canvas.getScaleY() / 1.1);
        });
        this.enlargeMap.setOnAction(e -> {
            double nextHeight = this.canvas.getHeight() * 1.1,
                    nextWidth = this.canvas.getWidth() * 1.1;
            this.editControl.processEnlargeMap(nextHeight, nextWidth);
            if (this.showGrid.isSelected()) {
                ObservableList<Node> nodes = this.canvas.getChildren();
                Node node;
                do {
                    node = nodes.get(0);
                    if (node instanceof Line) {
                        nodes.remove(0);
                    }
                } while (node instanceof Line);
                
                Line line;
                double height = this.canvas.getMaxHeight(), width = this.canvas.getMaxWidth();
                for (double i = 0; i < width; i += 24) {
                    line = new Line();
                    line.setStartX(i); line.setStartY(0);
                    line.setEndX(i); line.setEndY(height);
                    line.setStrokeWidth(1);
                    nodes.add(0, line);
                }
                for (double i = 0; i < height; i += 24) {
                    line = new Line();
                    line.setStartX(0); line.setStartY(i);
                    line.setEndX(width); line.setEndY(i);
                    line.setStrokeWidth(1);
                    nodes.add(0, line);
                }
            }
            this.shrinkMap.setDisable(false);
            
            this.setUndoRedoDisable();
        });
        this.shrinkMap.setOnAction(e -> {
            if (this.canvas.getHeight() > 200 && this.canvas.getWidth() > 200) {
                double nextHeight = this.canvas.getHeight() * 0.9,
                        nextWidth = this.canvas.getWidth() * 0.9;
                nextHeight = nextHeight > 200 ? nextHeight : 200;
                nextWidth = nextWidth > 200 ? nextWidth : 200;
                this.editControl.processShrinkMap(nextHeight, nextWidth);
                if (nextHeight == 200 || nextWidth == 200) {
                    this.shrinkMap.setDisable(true);
                }
                if (this.showGrid.isSelected()) {
                    ObservableList<Node> nodes = this.canvas.getChildren();
                    Node node;
                    do {
                        node = nodes.get(0);
                        if (node instanceof Line) {
                            nodes.remove(0);
                        }
                    } while (node instanceof Line);

                    Line line;
                    double height = this.canvas.getMaxHeight(), width = this.canvas.getMaxWidth();
                    for (double i = 0; i < width; i += 24) {
                        line = new Line();
                        line.setStartX(i); line.setStartY(0);
                        line.setEndX(i); line.setEndY(height);
                        line.setStrokeWidth(1);
                        nodes.add(0, line);
                    }
                    for (double i = 0; i < height; i += 24) {
                        line = new Line();
                        line.setStartX(0); line.setStartY(i);
                        line.setEndX(width); line.setEndY(i);
                        line.setStrokeWidth(1);
                        nodes.add(0, line);
                    }
                }
                
                this.setUndoRedoDisable();
                this.enableEditControls();
            }
        });
    }
    public void addAllPaths(MetroStation station, ArrayList<MetroLine> paths, ArrayList<Draggable> path) {
        ArrayList<Draggable> pathClone;
        ArrayList<MetroStation> stops;
        for (MetroLine line : paths) {
            pathClone = (ArrayList<Draggable>) path.clone();
            if (!pathClone.contains(line)) {
                stops = line.getStations();
                pathClone.add(line);
                pathClone.add(station);
                if (this.map.containsKey(station)) {
                    if (this.map.get(station).size() > pathClone.size()) {
                        this.map.put(station, (ArrayList<Draggable>) pathClone.clone());
                    }
                } else {
                    this.map.put(station, (ArrayList<Draggable>) pathClone.clone());
                }
                System.out.println(this.map);
                MetroStation stat;
                if (line.isCircular()) {
                    int size = stops.size(), bound = (size / 2) + 1, startIndex = stops.indexOf(station);
                    for (int i = 1; i < bound; i++) {
                        stat = stops.get((startIndex + i) % size);
                        pathClone.add(stat);
                        if (this.map.containsKey(stat)) {
                            if (this.map.get(stat).size() > pathClone.size()) {
                                this.map.put(stat, (ArrayList<Draggable>) pathClone.clone());
                            }
                        } else {
                            this.map.put(stat, (ArrayList<Draggable>) pathClone.clone());
                        }
                    }
                    pathClone = (ArrayList<Draggable>) path.clone();
                    pathClone.add(line); pathClone.add(station);
                    for (int i = -1; i >= bound * -1; i--) {
                        stat = stops.get(startIndex + i > 0 ? startIndex + i : size + startIndex + i);
                        pathClone.add(stat);
                        if (this.map.containsKey(stat)) {
                            if (this.map.get(stat).size() > pathClone.size()) {
                                this.map.put(stat, (ArrayList<Draggable>) pathClone.clone());
                            }
                        } else {
                            this.map.put(stat, (ArrayList<Draggable>) pathClone.clone());
                        }
                    }
                } else {
                    for (int i = stops.indexOf(station) + 1; i < stops.size(); i++) {
                        stat = stops.get(i);
                        pathClone.add(stat);
                        if (this.map.containsKey(stat)) {
                            if (this.map.get(stat).size() > pathClone.size()) {
                                this.map.put(stat, (ArrayList<Draggable>) pathClone.clone());
                            }
                        } else {
                            this.map.put(stat, (ArrayList<Draggable>) pathClone.clone());
                        }
                        System.out.println(this.map);
                    }
                    pathClone = (ArrayList<Draggable>) path.clone();
                    pathClone.add(line);
                    pathClone.add(station);
                    for (int i = stops.indexOf(station) - 1; i > -1; i--) {
                        stat = stops.get(i);
                        pathClone.add(stat);
                        if (this.map.containsKey(stat)) {
                            if (this.map.get(stat).size() > pathClone.size()) {
                                this.map.put(stat, (ArrayList<Draggable>) pathClone.clone());
                            }
                        } else {
                            this.map.put(stat, (ArrayList<Draggable>) pathClone.clone());
                        }
                        System.out.println(this.map);
                    }
                }
                pathClone = (ArrayList<Draggable>) path.clone();
                pathClone.add(line);
                pathClone.add(station);
                for (MetroStation stat0 : stops) {
                    if (stat0.getLines().size() > 1) {
                        ArrayList<MetroLine> nextPaths = (ArrayList<MetroLine>) stat0.getLines().clone();
                        nextPaths.remove(line);
                        this.addAllPaths(stat0, nextPaths, pathClone);
                    }
                }
            }            
        }
    }
    public void enableEditControls() {
        if (this.dataManager.isInState(SELECTED_NOTHING)) {
            this.setLineEditDisable(true);
            this.setStationEditDisable(true);
            this.setTextEditDisable(true);
            
            this.lines.getSelectionModel().select(null);
            this.stations.getSelectionModel().select(null);
            
            this.removeElement.setDisable(true);
        } else if (this.dataManager.isInState(SELECTED_ELEMENT)) {
            Shape selected = this.dataManager.getSelected();
            if (selected instanceof MetroLine) {
                this.setLineEditDisable(false);
                this.setStationEditDisable(true);
                this.setTextEditDisable(true);
                
                this.lines.getSelectionModel().select(((MetroLine) selected).getName());
                
                this.removeElement.setDisable(false);
            } else if (selected instanceof MetroStation) {
                this.setLineEditDisable(true);
                this.setStationEditDisable(false);
                this.setTextEditDisable(true);
                
                this.stations.getSelectionModel().select(((MetroStation) selected).getName());
                
                this.removeElement.setDisable(false);
            } else if (selected instanceof MetroLabel) {
                this.setLineEditDisable(true);
                this.setStationEditDisable(true);
                this.setTextEditDisable(false);
                
                this.lines.getSelectionModel().select(null);
                this.stations.getSelectionModel().select(null);
                
                this.removeElement.setDisable(((MetroLabel) selected).hasParent());
            } else if (selected instanceof MetroImage) {
                this.setLineEditDisable(true);
                this.setStationEditDisable(true);
                this.setTextEditDisable(true);
                
                this.lines.getSelectionModel().select(null);
                this.stations.getSelectionModel().select(null);
                
                this.removeElement.setDisable(false);
            }
        } else if (this.dataManager.isInState(SELECTED_START) || 
                this.dataManager.isInState(SELECTED_END)) {
            this.setLineEditDisable(true);
            this.setStationEditDisable(true);
            this.setTextEditDisable(true);
            
            this.snap.setDisable(false);
        } else if (this.dataManager.isInState(DRAGGING_ELEMENT)) {
            this.setLineEditDisable(true);
            this.setStationEditDisable(true);
            this.setTextEditDisable(true);
        } else if (this.dataManager.isInState(DRAGGING_START) || 
                this.dataManager.isInState(DRAGGING_END)) {
            this.setLineEditDisable(true);
            this.setStationEditDisable(true);
            this.setTextEditDisable(true);
        } else if (this.dataManager.isInState(ADDING_STATION) || 
                this.dataManager.isInState(REMOVING_STATION)) {
            this.setLineEditDisable(true);
            this.setStationEditDisable(true);
            this.setTextEditDisable(true);
        }
    }
    public void setLineEditDisable(boolean disable) {
        if (disable) {
            this.editLine.setDisable(true);
            this.editLine.setBackground(Background.EMPTY);
            this.removeLine.setDisable(true);
            this.lineAddStation.setDisable(true);
            this.lineRemoveStation.setDisable(true);
            this.lineListStations.setDisable(true);
            this.lineSetCircular.setDisable(true);
            this.lineWidth.setDisable(true);
        } else {
            MetroLine line = (MetroLine) this.dataManager.getSelected();
            this.addLine.setDisable(false);
            this.editLine.setDisable(false);
            this.editLine.setBackground(
                    new Background(
                    new BackgroundFill(line.getColor(), null, null)));
            this.removeLine.setDisable(false);
            this.lineAddStation.setDisable(line.isCircular());
            this.lineRemoveStation.setDisable(line.isCircular() || line.getStations().isEmpty());
            this.lineListStations.setDisable(line.getStations().isEmpty());
            this.lineSetCircular.setDisable(false);
            this.lineWidth.setDisable(false);
            this.lineWidth.setValue(line.getStrokeWidth());
        }
    }
    public void setStationEditDisable(boolean disable) {
        if (disable) {
            this.editStation.setDisable(true);
            this.editStation.setBackground(Background.EMPTY);
            this.removeStation.setDisable(true);
            this.snap.setDisable(true);
            this.placeLabel.setDisable(true);
            this.rotateLabel.setDisable(true);
            this.radius.setDisable(true);
        } else {
            MetroStation station = (MetroStation) this.dataManager.getSelected();
            this.addStation.setDisable(false);
            this.editStation.setDisable(false);
            this.editStation.setBackground(
                    new Background(
                    new BackgroundFill(station.getColor(), null, null)));
            this.removeStation.setDisable(false);
            this.snap.setDisable(false);
            this.placeLabel.setDisable(false);
            this.rotateLabel.setDisable(false);
            this.radius.setDisable(false);
            this.radius.setValue(station.getRadius());
        }
    }
    public void setTextEditDisable(boolean disable) {
        if (disable) {
            this.fontColor.setDisable(true);
            this.bold.setDisable(true);
            this.italic.setDisable(true);
            this.fontFamily.setDisable(true);
            this.fontFamily.getSelectionModel().select(null);
            this.fontSize.setDisable(true);
            this.fontSize.getSelectionModel().select(null);
        } else {
            MetroLabel selected = (MetroLabel) this.dataManager.getSelected();
            this.fontColor.setDisable(false);
            this.fontColor.setValue(selected.getTextFill());
            this.bold.setDisable(false);
            this.italic.setDisable(false);
            this.fontFamily.setDisable(false);
            this.fontFamily.setValue(selected.getFamily());
            this.fontSize.setDisable(false);
            this.fontSize.setValue(selected.getFontSize());
        }
    }
    public void setUndoRedoDisable() {
        this.undo.setDisable(this.transactions.undoDisable());
        this.redo.setDisable(this.transactions.redoDisable());
    }
    public void initStyle() {
        // Apply AppGUI CSS to buttons added in top toolbar
        this.programButtons.getStyleClass().add(CLASS_BORDERED_PANE);
        
        // Apply Metro Map Maker CSS to canvas and editing controls
        this.canvas.getStyleClass().add(CLASS_RENDER_CANVAS);
        
        this.editToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR);
        
        this.lineRow1.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        this.lineLabel.getStyleClass().add(CLASS_CONTROL_LABEL);
        
        this.stationRow2.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        this.stationLabel.getStyleClass().add(CLASS_CONTROL_LABEL);
        
        this.routeRow3.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        
        this.decorRow4.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        this.decorLabel.getStyleClass().add(CLASS_CONTROL_LABEL);
        this.backgroundColor.getStyleClass().add(CLASS_COLOR_CHOOSER_PANE);
        
        this.fontRow5.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        this.fontLabel.getStyleClass().add(CLASS_CONTROL_LABEL);
        this.fontColor.getStyleClass().add(CLASS_COLOR_CHOOSER_PANE);
        
        this.navRow6.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        this.navLabel.getStyleClass().add(CLASS_CONTROL_LABEL);
    }
    @Override
    public void resetWorkspace() {
        // Reset MetroLine editing controls
        this.lines.getItems().clear();
        this.lines.getSelectionModel().clearSelection();
        this.editLine.setDisable(true);
        this.editLine.setBackground(Background.EMPTY);
        this.removeLine.setDisable(true);
        this.lineAddStation.setDisable(true);
        this.lineRemoveStation.setDisable(true);
        this.lineListStations.setDisable(true);
        this.lineWidth.setDisable(true);
        
        // Reset MetroStation editing controls
        this.stations.getItems().clear();
        this.stations.getSelectionModel().clearSelection();
        this.editStation.setDisable(true);
        this.editStation.setBackground(Background.EMPTY);
        this.removeStation.setDisable(true);
        this.snap.setDisable(true);
        this.placeLabel.setDisable(true);
        this.rotateLabel.setDisable(true);
        this.radius.setDisable(true);
        
        // Reset route finding controls        
        this.start.getItems().clear();
        this.start.getSelectionModel().clearSelection();
        this.end.getItems().clear();
        this.end.getSelectionModel().clearSelection();
        this.findRoute.setDisable(false);
        
        // Reset decor editing controls
        this.backgroundColor.setValue(DEFAULT_BACKGROUND_COLOR);
        this.setImageBG.setDisable(false);
        this.addImage.setDisable(false);
        this.addLabel.setDisable(false);
        this.removeElement.setDisable(true);
        
        // Reset text editing controls
        this.bold.setDisable(true);
        this.italic.setDisable(true);
        this.fontFamily.getItems().clear();
        this.fontFamily.getSelectionModel().clearSelection();
        for (String fontName : Font.getFamilies()) {
            this.fontFamily.getItems().add(fontName);
        }
        this.fontFamily.setVisibleRowCount(5);
        this.fontSize.getItems().clear();
        this.fontSize.getSelectionModel().clearSelection();
        for (int i = 12; i <= 36; i++) {
            this.fontSize.getItems().add((double) i);
        }
        this.fontColor.setValue(Color.BLACK);
        
        // Reset navigation controls
        this.zoomIn.setDisable(false);
        this.zoomOut.setDisable(false);
        // Reset current zoom
        this.enlargeMap.setDisable(false);
        this.shrinkMap.setDisable(false);
        this.showGrid.setAllowIndeterminate(false);
        this.showGrid.setSelected(false);
        // Remove grid lines
    }
    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        this.dataManager = (metMapData) dataComponent;
        this.canvas.setMaxHeight(this.dataManager.getCanvasHeight());
        this.canvas.setMinHeight(this.dataManager.getCanvasHeight());
        this.canvas.setMaxWidth(this.dataManager.getCanvasWidth());
        this.canvas.setMinWidth(this.dataManager.getCanvasWidth());
        this.canvas.setBackground(
                new Background(
                new BackgroundFill(
                    this.dataManager.getBackground(), null, null)));
        this.lines.getItems().clear();
        ArrayList<MetroLine> lineList = this.dataManager.getLines();
        for (MetroLine line : lineList) {
            this.lines.getItems().add(line.getName());
        }
        this.stations.getItems().clear();
        this.start.getItems().clear();
        this.end.getItems().clear();
        ArrayList<MetroStation> stationList = this.dataManager.getStations();
        String name;
        for (MetroStation station : stationList) {
            name = station.getName();
            this.stations.getItems().add(name);
            this.start.getItems().add(name);
            this.end.getItems().add(name);
        }
    }
}
