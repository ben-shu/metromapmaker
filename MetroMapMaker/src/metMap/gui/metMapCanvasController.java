package metMap.gui;

import djf.AppTemplate;

import metMap.data.metMapData;
import metMap.data.Draggable;
import metMap.data.MetroLine;
import metMap.data.MetroLine.MetroLineEnd;
import metMap.data.MetroStation;
import metMap.data.MetroImage;
import metMap.data.MetroLabel;

import metMap.data.jTPS;
import metMap.data.Line_AddStation;
import metMap.data.Line_RemoveStation;
import metMap.data.Node_Dragging;
import metMap.data.Node_Dragged;

import static metMap.data.metMapState.*;
import static metMap.data.MetroLine.START;
import static metMap.data.MetroLine.END;

import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.shape.Shape;

/**
 * Controller class designed to handle user input events in
 * the Metro Map Maker UI.
 * 
 * @author Benjamin Shu
 */
public class metMapCanvasController {
    private final AppTemplate app;
    private final metMapData dataManager;
    private final jTPS transactions;
    private double xOff, yOff;
    private double prevX, prevY;
    
    protected metMapCanvasController(AppTemplate app) {
        this.app = app;
        this.dataManager = (metMapData) this.app.getDataComponent();
        this.transactions = this.dataManager.getTransactions();
        this.xOff = 0; this.yOff = 0;
    }
    public void processMousePress(double x, double y) {
        if (this.dataManager.isInState(SELECTED_NOTHING)) {
            Shape selected = this.dataManager.selectTopShape(x, y);
            if (selected != null) {
                if (selected instanceof Draggable) {
                    this.prevX = ((Draggable)selected).getX();
                    this.prevY = ((Draggable)selected).getY();
                    this.xOff = this.prevX - x;
                    this.yOff = this.prevY - y;
                    if (selected instanceof MetroLine) {
                        MetroLine line = (MetroLine) selected;
                        if (line.startContains(x, y)) {
                            if (line.isCircular()) {
                                this.dataManager.setState(SELECTED_ELEMENT);
                            } else {
                                line.addStartHighlight();
                                line.removeEndHighlight();
                                this.dataManager.selectShape(line.getStartHighlight());
                                this.prevX = line.getStartHighlight().getCenterX();
                                this.prevY = line.getStartHighlight().getCenterY();
                                this.xOff = this.prevX - x;
                                this.yOff = this.prevY - y;
                                this.dataManager.setState(SELECTED_START);
                            }
                        } else if (line.endContains(x, y)) {
                            if (line.isCircular()) {
                                this.dataManager.setState(SELECTED_ELEMENT);
                            } else {
                                line.addEndHighlight();
                                line.removeStartHighlight();
                                this.dataManager.selectShape(line.getEndHighlight());
                                this.prevX = line.getEndHighlight().getCenterX();
                                this.prevY = line.getEndHighlight().getCenterY();
                                this.xOff = this.prevX - x;
                                this.yOff = this.prevY - y;
                                this.dataManager.setState(SELECTED_END);
                            }
                        } else {
                            line.removeStartHighlight();
                            line.removeEndHighlight();
                            this.dataManager.setState(SELECTED_ELEMENT);
                        }
                    } else {
                        this.dataManager.setState(SELECTED_ELEMENT);
                    }
                }
            }
        } else if (this.dataManager.isInState(SELECTED_ELEMENT)) {
            Shape selected = this.dataManager.selectTopShape(x, y);
            if (selected == null) {
                this.dataManager.setState(SELECTED_NOTHING);
            } else {
                if (selected instanceof Draggable) {
                    this.prevX = ((Draggable)selected).getX();
                    this.prevY = ((Draggable)selected).getY();
                    this.xOff = this.prevX - x;
                    this.yOff = this.prevY - y;
                    if (selected instanceof MetroLine) {
                        MetroLine line = (MetroLine) selected;
                        if (line.startContains(x, y)) {
                            line.addStartHighlight();
                            line.removeEndHighlight();
                            this.prevX = line.getStartHighlight().getCenterX();
                            this.prevY = line.getStartHighlight().getCenterY();
                            this.xOff = this.prevX - x;
                            this.yOff = this.prevY - y;
                            this.dataManager.selectShape(line.getStartHighlight());
                            this.dataManager.setState(SELECTED_START);
                        } else if (line.endContains(x, y)) {
                            line.addEndHighlight();
                            line.removeStartHighlight();
                            this.prevX = line.getEndHighlight().getCenterX();
                            this.prevY = line.getEndHighlight().getCenterY();
                            this.xOff = this.prevX - x;
                            this.yOff = this.prevY - y;
                            this.dataManager.selectShape(line.getEndHighlight());
                            this.dataManager.setState(SELECTED_END);
                        } else {
                            this.dataManager.setState(SELECTED_ELEMENT);
                        }
                    } else {
                        this.dataManager.setState(SELECTED_ELEMENT);
                    }
                }
            }
        } else if (this.dataManager.isInState(ADDING_STATION)) {
            Shape selected = this.dataManager.getTopShape(x, y);
            if (selected == null) {
                this.dataManager.setState(SELECTED_ELEMENT);
            } else {
                if (!(selected instanceof MetroStation)) {
                    this.dataManager.setState(SELECTED_ELEMENT);
                } else {
                    MetroLine line = (MetroLine) this.dataManager.getSelected();
                    MetroStation station = (MetroStation) selected;
                    Line_AddStation transaction = new Line_AddStation(this.dataManager, line, station);
                    this.transactions.addTransaction(transaction);
                    this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
                }
            }
        } else if (this.dataManager.isInState(REMOVING_STATION)) {
            Shape selected = this.dataManager.getTopShape(x, y);
            if (selected == null) {
                this.dataManager.setState(SELECTED_ELEMENT);
            } else {
                if (!(selected instanceof MetroStation)) {
                    this.dataManager.setState(SELECTED_ELEMENT);
                } else {
                    MetroLine line = (MetroLine) this.dataManager.getSelected();
                    MetroStation station = (MetroStation) selected;
                    Line_RemoveStation transaction = new Line_RemoveStation(this.dataManager, line, station);
                    this.transactions.addTransaction(transaction);
                    this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
                }
            }
        } else if (this.dataManager.isInState(SELECTED_START)) {
            Shape selected = this.dataManager.getTopShape(x, y);
            if (selected == null) {
                MetroLine line = ((MetroLineEnd) this.dataManager.getSelected()).getLine();
                line.removeStartHighlight();
                line.removeEndHighlight();
                this.dataManager.deselect();
                this.dataManager.setState(SELECTED_NOTHING);
            } else {
                if (selected instanceof Draggable) {
                    this.prevX = ((Draggable) selected).getX();
                    this.prevY = ((Draggable) selected).getY();
                    this.xOff = this.prevX - x;
                    this.yOff = this.prevY - y;
                    if (selected instanceof MetroLineEnd) {
                        MetroLineEnd start = (MetroLineEnd) this.dataManager.getSelected(),
                                next = (MetroLineEnd) selected;
                        this.dataManager.selectShape(next);
                        if (next != start) {
                            start.getLine().removeStartHighlight();
                            this.dataManager.setState(next.getType().equals(START)
                                    ? SELECTED_START : SELECTED_END);
                        }
                    } else {
                        MetroLine line = ((MetroLineEnd) this.dataManager.getSelected()).getLine();
                        line.removeStartHighlight();
                        line.removeEndHighlight();
                        this.prevX = ((Draggable) selected).getX();
                        this.prevY = ((Draggable) selected).getY();
                        this.xOff = this.prevX - x;
                        this.yOff = this.prevY - y;
                        this.dataManager.selectShape(selected);
                        this.dataManager.setState(SELECTED_ELEMENT);
                    }
                }
            }
        } else if (this.dataManager.isInState(SELECTED_END)) {
            Shape selected = this.dataManager.getTopShape(x, y);
            if (selected == null) {
                MetroLine line = ((MetroLineEnd) this.dataManager.getSelected()).getLine();
                line.removeStartHighlight();
                line.removeEndHighlight();
                this.dataManager.setState(SELECTED_NOTHING);
            } else {
                if (selected instanceof Draggable) {
                    this.prevX = ((Draggable) selected).getX();
                    this.prevY = ((Draggable) selected).getY();
                    this.xOff = this.prevX - x;
                    this.yOff = this.prevY - y;
                    if (selected instanceof MetroLineEnd) {
                        MetroLineEnd end = (MetroLineEnd) this.dataManager.getSelected(),
                                next = (MetroLineEnd) selected;
                        this.dataManager.selectShape(next);
                        if (next != end) {
                            end.getLine().removeEndHighlight();
                            this.dataManager.setState(next.getType().equals(START)
                                    ? SELECTED_START : SELECTED_END);
                        }
                    } else {
                        MetroLine line = ((MetroLineEnd) this.dataManager.getSelected()).getLine();
                        line.removeStartHighlight();
                        line.removeEndHighlight();
                        this.prevX = ((Draggable) selected).getX();
                        this.prevY = ((Draggable) selected).getY();
                        this.xOff = this.prevX - x;
                        this.yOff = this.prevY - y;
                        this.dataManager.selectShape(selected);
                        this.dataManager.setState(SELECTED_ELEMENT);
                    }
                }
            }
        }
    }
    public void processMouseDragged(double x, double y) {
        Shape selected = this.dataManager.getSelected();
        if (this.dataManager.isInState(SELECTED_ELEMENT)) {
            if (selected == null) {
                this.dataManager.setState(SELECTED_NOTHING);
            } else {
                if (selected instanceof Draggable) {
                    if (selected instanceof MetroLine) {
                        MetroLine selectedLine = (MetroLine) selected;
                        selectedLine.removeStartHighlight();
                        selectedLine.removeEndHighlight();
                        selectedLine.drag(x, y, this.xOff, this.yOff);
                        this.dataManager.setState(DRAGGING_ELEMENT);
                    } else if (selected instanceof MetroLabel) {
                        if (!((MetroLabel) selected).hasParent()) {
                            ((Draggable) selected).drag(x, y, this.xOff, this.yOff);
                            this.dataManager.setState(DRAGGING_ELEMENT);
                        }
                    } else {
                        ((Draggable) selected).drag(x, y, this.xOff, this.yOff);
                        this.dataManager.setState(DRAGGING_ELEMENT);
                    }
                }
            }
        } else if (this.dataManager.isInState(SELECTED_START)) {
            ((Draggable) selected).drag(x, y, this.xOff, this.yOff);
            this.dataManager.setState(DRAGGING_START);
        } else if (this.dataManager.isInState(SELECTED_END)) {
            ((Draggable) selected).drag(x, y, this.xOff, this.yOff);
            this.dataManager.setState(DRAGGING_END);
        } else if (this.dataManager.isInState(DRAGGING_ELEMENT)) {
            ((Draggable) selected).drag(x, y, this.xOff, this.yOff);
        } else if (this.dataManager.isInState(DRAGGING_START)) {
            ((Draggable) selected).drag(x, y, this.xOff, this.yOff);
        } else if (this.dataManager.isInState(DRAGGING_END)) {
            ((Draggable) selected).drag(x, y, this.xOff, this.yOff);
        }
    }
    public void processMouseReleased(double x, double y) {
        if (this.dataManager.isInState(DRAGGING_ELEMENT) || 
                this.dataManager.isInState(DRAGGING_START) ||
                this.dataManager.isInState(DRAGGING_END)) {
            if (this.dataManager.isInState(DRAGGING_ELEMENT)) {
                this.dataManager.setState(SELECTED_ELEMENT);
            } else if (this.dataManager.isInState(DRAGGING_START)) {
                this.dataManager.setState(SELECTED_START);
            } else if (this.dataManager.isInState(DRAGGING_END)) {
                this.dataManager.setState(SELECTED_END);
            }
            Node_Dragging transaction = new Node_Dragging(
                    x, y, this.xOff, this.yOff, this.prevX, this.prevY, 
                    (Draggable) this.dataManager.getSelected());
            this.transactions.addTransaction(transaction);
            this.app.getGUI().getFileController().markAsEdited(this.app.getGUI());
        }
    }
}
