package metMap;

import djf.AppTemplate;
import djf.ui.AppTextInputDialogSingleton;
import djf.ui.AppWelcomeDialogSingleton;
import metMap.file.metMapFiles;
import metMap.data.metMapData;
import metMap.gui.metMapWorkspace;

import static djf.settings.AppStartupConstants.PATH_WORK;
import java.nio.file.Files;
import java.io.File;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.DirectoryStream;
import java.nio.file.DirectoryIteratorException;
import java.io.IOException;

import java.util.Locale;
import static javafx.application.Application.launch;

/**
 * Main application class for Metro Map Maker.
 * 
 * @author Benjamin Shu
 */
public class MetroMapMakerApp extends AppTemplate {
    @Override
    public void buildAppComponentsHook() {
        // Build the app.
        fileComponent = new metMapFiles(this);
        dataComponent = new metMapData(this);
        workspaceComponent = new metMapWorkspace(this);
        
        File[] files = new File[5];
        Path dir = Paths.get(PATH_WORK);
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
            File file;
            for (Path path : stream) {
                file = path.toFile();
                for (int i = 0; i < 5; i++) {
                    if (files[i] == null) {
                        files[i] = file;
                        break;
                    } else {
                        if (file.lastModified() > files[i].lastModified()) {
                            for (int j = 4; j > i; j--) {
                                files[j] = files[j-1];
                            }
                            files[i] = file;
                            break;
                        }
                    }
                }
            }
        } catch (IOException | DirectoryIteratorException x) {  }
        
        // Welcome dialog box used upon startup, with supporting text input box.
        AppWelcomeDialogSingleton welcomeDialog = AppWelcomeDialogSingleton.getSingleton();
        AppTextInputDialogSingleton textInputDialog = AppTextInputDialogSingleton.getSingleton();
        welcomeDialog.init(this);
        for (int i = 0; i < 5; i++) {
            if (files[i] != null) {
                welcomeDialog.addRecentHyperink(files[i]);
            }
        }
        welcomeDialog.showDialog();
    }
    public static void main(String[] args) {
	Locale.setDefault(Locale.US);
	launch(args);
    }
}
