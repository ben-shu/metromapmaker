package metMap.data;

import javafx.scene.shape.Rectangle;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;

/**
 *
 * @author Benjamin Shu
 */
public class MetroImage extends Rectangle implements Draggable {
    private String imageURI;
    private ImagePattern imageFill;
    public MetroImage(String imageURI) {
        super();
        this.imageURI = imageURI;
        Image image = new Image(imageURI);
        this.setHeight(image.getHeight());
        this.setWidth(image.getWidth());
        this.imageFill = new ImagePattern(image);
        this.setFill(this.imageFill);
    }
    public String getImageURI() {
        return this.imageURI;
    }
    @Override
    public void drag(double x, double y, double xOff, double yOff) {
        this.setX(x + xOff); this.setY(y + yOff);
    }
    @Override
    public String getType() {
        return Draggable.IMAGE;
    }
}
