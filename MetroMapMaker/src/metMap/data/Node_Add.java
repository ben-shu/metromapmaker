package metMap.data;

import static metMap.data.metMapState.*;
/**
 * Transaction class designed to enable adding map elements.
 * 
 * @author Benjamin Shu
 */
public class Node_Add implements jTPS_Transaction {
    private metMapData dataManager;
    // Stored Draggable element to keep transaction simple
    private Draggable node;
    
    public Node_Add(metMapData dataManager, Draggable node) {
        this.dataManager = dataManager;
        this.node = node;
    }
    @Override
    public void doTransaction() {
        if (this.node instanceof MetroLine) {
            MetroLine line = (MetroLine) this.node;
            this.dataManager.addShape(line);
            this.dataManager.addShape(line.getStartHighlight());
            this.dataManager.addShape(line.getEndHighlight());
            this.dataManager.addShape(line.getStartLabel());
            this.dataManager.addShape(line.getEndLabel());
        } else if (this.node instanceof MetroStation) {
            MetroStation station = (MetroStation) this.node;
            this.dataManager.addShape(station);
            this.dataManager.addShape(station.getLabel());
        } else if (this.node instanceof MetroLabel) {
            MetroLabel label = (MetroLabel) this.node;
            this.dataManager.addShape(label);
        } else if (this.node instanceof MetroImage) {
            MetroImage image = (MetroImage) this.node;
            this.dataManager.addShape(image);
        }
        this.dataManager.selectLastAdded();
        this.dataManager.setState(SELECTED_ELEMENT);
    }
    @Override
    public void undoTransaction() {
        if (this.node instanceof MetroLine) {
            MetroLine line = (MetroLine) this.node;
            this.dataManager.removeShape(line);
            this.dataManager.removeShape(line.getStartHighlight());
            this.dataManager.removeShape(line.getEndHighlight());
            this.dataManager.removeShape(line.getStartLabel());
            this.dataManager.removeShape(line.getEndLabel());
        } else if (this.node instanceof MetroStation) {
            MetroStation station = (MetroStation) this.node;
            this.dataManager.removeShape(station);
            this.dataManager.removeShape(station.getLabel());
        } else if (this.node instanceof MetroLabel) {
            MetroLabel label = (MetroLabel) this.node;
            this.dataManager.removeShape(label);
        } else if (this.node instanceof MetroImage) {
            MetroImage image = (MetroImage) this.node;
            this.dataManager.removeShape(image);
        }
        this.dataManager.deselect();
        this.dataManager.setState(SELECTED_NOTHING);
    }
}
