package metMap.data;

import javafx.scene.shape.Circle;

/**
 *
 * @author Benjamin Shu
 */
public class Node_Snap implements jTPS_Transaction {
    private Circle node;
    private double prevX, prevY, nextX, nextY;
    public Node_Snap(Circle node) {
        this.node = node;
        this.prevX = node.getCenterX(); this.prevY = node.getCenterY();
        this.nextX = (this.prevX % 24 > 12) ? 
                24 * (int)((this.prevX / 24) + 1) : 24 * (int)(this.prevX / 24);
        this.nextY = (this.prevY % 24 > 12) ? 
                24 * (int)((this.prevY / 24) + 1) : 24 * (int)(this.prevY / 24);
    }
    @Override
    public void doTransaction() {
        this.node.setCenterX(this.nextX);
        this.node.setCenterY(this.nextY);
    }
    @Override
    public void undoTransaction() {
        this.node.setCenterX(this.prevX);
        this.node.setCenterY(this.prevY);
    }
}
