package metMap.data;

/**
 *
 * @author Benjamin Shu
 */
public class Node_AdjustWidth implements jTPS_Transaction {
    private Draggable node;
    private double prevWidth, nextWidth;
    
    public Node_AdjustWidth(Draggable node, double nextWidth) {
        this.node = node;
        if (this.node instanceof MetroStation) { 
            this.prevWidth = ((MetroStation) this.node).getRadius();
        } else if (this.node instanceof MetroLine) {
            this.prevWidth = ((MetroLine) this.node).getStrokeWidth();
        }
        this.nextWidth = nextWidth;
    }
    @Override
    public void doTransaction() {
        if (this.node instanceof MetroStation) {
            ((MetroStation) this.node).adjustRadius(this.nextWidth);
        } else if (this.node instanceof MetroLine) {
            ((MetroLine) this.node).adjustWidth(this.nextWidth);
        }
    }
    @Override
    public void undoTransaction() {
        if (this.node instanceof MetroStation) {
            ((MetroStation) this.node).adjustRadius(this.prevWidth);
        } else if (this.node instanceof MetroLine) {
            ((MetroLine) this.node).adjustWidth(this.prevWidth);
        }
    }
}
