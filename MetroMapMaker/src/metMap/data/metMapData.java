package metMap.data;

import djf.AppTemplate;
import djf.components.AppDataComponent;

import metMap.gui.metMapWorkspace;
import static metMap.data.metMapState.*;

import javafx.scene.Node;
import javafx.scene.shape.Shape;
import javafx.collections.ObservableList;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Paint;
import javafx.scene.paint.Color;
import javafx.scene.effect.Effect;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;

import java.util.ArrayList;

/**
 * Data manager class designed to control adding, removing, and editing map 
 * elements in Metro Map Maker, as well as providing support for file saving 
 * and exporting.
 * 
 * @author Benjamin Shu
 */
public class metMapData implements AppDataComponent {
    // Static constants for the colors that Metro Map Maker will use.
    private static final String WHITE_HEX = "#FFFFFF";
    private static final String BLACK_HEX = "#000000";
    private static final String YELLOW_HEX = "#EEEE00";
    
    // Default line and station colors.
    public static final Color DEFAULT_BACKGROUND_COLOR = Color.valueOf(WHITE_HEX);
    public static final Color DEFAULT_LINE_COLOR = Color.valueOf(BLACK_HEX);
    public static final Color DEFAULT_STATION_FILL = Color.valueOf(WHITE_HEX);
    public static final Color DEFAULT_STATION_STROKE = Color.valueOf(BLACK_HEX);
    
    // Highlight color and thickness.
    public static final Color HIGHLIGHT_COLOR = Color.valueOf(YELLOW_HEX);
    private static final int HIGHLIGHT_THICKNESS = 5;
    
    // All of the map elements.
    private ObservableList<Node> elements;
    private ArrayList<MetroLine> lines;
    private ArrayList<MetroStation> stations;
    
    // Current dimensions of the map canvas
    private double canvasHeight, canvasWidth;
    
    // The background color/image currently in use, and the URI if background 
    // is currently an image.
    private Paint background;
    private String backgroundURI;
    
    // The currently selected map element.
    // Null if user has not selected anything/clicked on the canvas.
    private Shape selected;
    
    // Current state of data manager.
    private metMapState state;
    
    // Reference to its app.
    private AppTemplate app;
    
    // Highlight effect for selected map elements.
    private Effect highlight;
    
    // Transaction manager for edits made this session.
    private jTPS transactions;
    
    public metMapData(AppTemplate initApp) {
        this.app = initApp;
        this.selected = null;
        
        this.background = DEFAULT_BACKGROUND_COLOR;
        setState(SELECTED_NOTHING);
        
        this.lines = new ArrayList<MetroLine>();
        this.stations = new ArrayList<MetroStation>();
        this.transactions = new jTPS();
        
        DropShadow dropShadowEffect = new DropShadow();
	dropShadowEffect.setOffsetX(0.0f);
	dropShadowEffect.setOffsetY(0.0f);
	dropShadowEffect.setSpread(1.0);
	dropShadowEffect.setColor(HIGHLIGHT_COLOR);
	dropShadowEffect.setBlurType(BlurType.GAUSSIAN);
	dropShadowEffect.setRadius(HIGHLIGHT_THICKNESS);
	this.highlight = dropShadowEffect;
        
        this.canvasHeight = 600; this.canvasWidth = 1000;
    }
    public ArrayList<MetroLine> getLines() {
        return this.lines;
    }
    public ArrayList<MetroStation> getStations() {
        return this.stations;
    }
    public jTPS getTransactions() {
        return this.transactions;
    }
    public void setElements(ObservableList<Node> elements) {
        this.elements = elements;
    }
    public ObservableList<Node> getElements() {
        return this.elements;
    }
    public void addShape(Shape shape) {
        if (shape != null) {
            this.elements.add(shape);
            if (shape instanceof MetroLine) {
                this.lines.add((MetroLine) shape);
            } else if (shape instanceof MetroStation) {
                this.stations.add((MetroStation) shape);
            }
        }
    }
    public void addShape(int index, Shape shape) {
        if (shape != null) {
            this.elements.add(index, shape);
            if (shape instanceof MetroLine) {
                this.lines.add((MetroLine) shape);
            } else if (shape instanceof MetroStation) {
                this.stations.add((MetroStation) shape);
            }
        }
    }
    public void removeShape(Shape shape) {
        this.elements.remove(shape);
        if (shape instanceof MetroLine) {
            this.lines.remove((MetroLine) shape);
        } else if (shape instanceof MetroStation) {
            this.stations.remove((MetroStation) shape);
        }
    }
    public void moveStationToLine(MetroStation src, MetroLine dest) {
        if (this.elements.contains(src) && this.elements.contains(dest)) {
            this.elements.remove(src);
            this.elements.remove(src.getLabel());
            int index = this.elements.indexOf(dest) + 1;
            this.elements.add(index, src.getLabel());
            this.elements.add(index, src);
        }
    }
    public double getCanvasHeight() {
        return this.canvasHeight;
    }
    public void setCanvasHeight(double canvasHeight) {
        this.canvasHeight = canvasHeight;
    }
    public double getCanvasWidth() {
        return this.canvasWidth;
    }
    public void setCanvasWidth(double canvasWidth) {
        this.canvasWidth = canvasWidth;
    }
    public Paint getDefaultBackgroundColor() {
        return DEFAULT_BACKGROUND_COLOR;
    }
    public Paint getDefaultLineColor() {
        return DEFAULT_LINE_COLOR;
    }
    public Paint getDefaultStationFill() {
        return DEFAULT_STATION_FILL;
    }
    public Paint getDefaultStationStroke() {
        return DEFAULT_STATION_STROKE;
    }
    public Paint getBackground() {
        return this.background;
    }
    public String getBackgroundURI() {
        return this.backgroundURI;
    }
    public void setBackground(Paint background) {
        this.background = background;
        metMapWorkspace workspace = (metMapWorkspace)this.app.getWorkspaceComponent();
        workspace.getCanvas().setBackground(
                new Background(
                        new BackgroundFill(this.background, null, null)));
    }
    public void setBackgroundURI(String backgroundURI) {
        this.backgroundURI = backgroundURI;
    }
    public boolean isInState(metMapState state) {
        return state == this.state;
    }
    public void setState(metMapState state) {
        this.state = state;
    }
    public Shape getSelected() {
        return this.selected;
    }
    private void highlightElement(Shape element) {
        element.setEffect(this.highlight);
    }
    private void unhighlightElement(Shape element) {
        element.setEffect(null);
    }
    public void deselect() {
        if (this.selected != null) {
            this.selected = null;
        }
        for (Node element : this.elements) {
            this.unhighlightElement((Shape) element);
        }
    }
    public Shape selectTopShape(double x, double y) {
        // What shape did the user click (if any)?
        Shape shape = getTopShape(x, y);
        // If they clicked the selected shape, do nothing.
        if (shape == this.selected) {
            if (this.selected != null) { highlightElement(this.selected); }
            return this.selected;
        }
        // If they clicked somewhere else, unhighlight the currently selected shape.
        if (this.selected != null) {
            unhighlightElement(this.selected);
        }
        // If they clicked a different shape, highlight that shape.
        if (shape != null && shape instanceof Draggable) {
            highlightElement(shape);
        }
        this.selected = shape;
        if (this.selected instanceof MetroLine) {
            ((metMapWorkspace) this.app.getWorkspaceComponent())
                    .setSelectedLine(((MetroLine) this.selected).getName());
        } else if (this.selected instanceof MetroStation) {
            ((metMapWorkspace) this.app.getWorkspaceComponent())
                    .setSelectedStation(((MetroStation) this.selected).getName());
        }
        return shape;
    }
    public void selectShape(Shape shape) {
        if (this.selected != null) {
            unhighlightElement(this.selected);
        }
        highlightElement(shape);
        this.selected = shape;
    }
    public Shape getTopShape(double x, double y) {
        Shape shape = null;
        for (int i = this.elements.size() - 1; i >= 0; i--) {
            shape = (Shape) this.elements.get(i);
            if (shape.contains(x, y)) {
                return shape;
            }
        }
        shape = null;
        return shape;
    }
    public void selectLastAdded() {
        Shape last = null;
        if (!this.elements.isEmpty()) {
            for (int i = this.elements.size() - 1; i > -1; i--) {
                last = (Shape) this.elements.get(i);
                if (last instanceof MetroLine || 
                        last instanceof MetroStation || 
                        last instanceof MetroImage) {
                    break;
                } else if (last instanceof MetroLabel) {
                    if (!((MetroLabel) last).hasParent()) {
                        break;
                    }
                }
            }
            if (last != null) { selectShape(last); }
        }
    }
    @Override
    public void resetData() {
        this.setState(metMapState.SELECTED_NOTHING);
	this.selected = null;
        
        this.canvasHeight = 600;
        this.canvasWidth = 1000;
        
        this.background = DEFAULT_BACKGROUND_COLOR;
        
	this.elements.clear();
        this.lines.clear();
        this.stations.clear();
	((metMapWorkspace)app.getWorkspaceComponent()).getCanvas().getChildren().clear();
    }
}
