package metMap.data;

/**
 * Interface used for draggable map elements in Metro Map Maker.
 * 
 * @author Benjamin Shu
 */
public interface Draggable {
    // The four types of Draggable elements
    public static String LINE = "Line";
    public static String LINE_END = "Line End";
    public static String STATION = "Station";
    public static String LABEL = "Label";
    public static String IMAGE = "Image";
    
    // Accessor methods for current location
    public double getX();
    public void setX(double x);
    public double getY();
    public void setY(double y);
    // Drag method for user-specified movement
    public void drag(double x, double y, double xOff, double yOff);
    // Simple identification method
    public String getType();
}
