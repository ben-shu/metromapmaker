package metMap.data;

import javafx.scene.paint.Color;

/**
 *
 * @author Benjamin Shu
 */
public interface TextEditable {
    public Color getTextFill();
    public void setTextFill(Color color);
    public String getFamily();
    public void setFamily(String family);
    public boolean isBold();
    public void toggleBold();
    public boolean isItalic();
    public void toggleItalic();
    public double getFontSize();
    public void setFontSize(double size);
}
