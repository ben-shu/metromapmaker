package metMap.data;

/**
 *
 * @author Benjamin Shu
 */
public class Line_RemoveStation implements jTPS_Transaction {
    private metMapData dataManager;
    private MetroLine line;
    private MetroStation station;
    
    public Line_RemoveStation(metMapData dataManager, MetroLine line, MetroStation station) {
        this.dataManager = dataManager;
        this.line = line;
        this.station = station;
    }
    @Override
    public void doTransaction() {
        this.line.removeStation(this.station);
    }
    @Override
    public void undoTransaction() {
        this.line.addStation(this.station);
        this.dataManager.moveStationToLine(this.station, this.line);
    }
}
