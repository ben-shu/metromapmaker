package metMap.data;

/**
 * Transaction class designed to enable user dragging of map elements.
 * 
 * @author Benjamin Shu
 */
public class Node_Dragging implements jTPS_Transaction {
    // Stored element
    private Draggable node;
    // Previous and current x- and y-coordinates.
    private double prevX, prevY, nextX, nextY, xOff, yOff;
    public Node_Dragging(double nextX, double nextY, 
            double xOff, double yOff,
            double prevX, double prevY,
            Draggable node) {
        this.node = node;
        this.prevX = prevX; this.prevY = prevY;
        this.nextX = nextX; this.nextY = nextY;
        this.xOff = xOff; this.yOff = yOff;
    }
    @Override
    public void doTransaction() {
        this.node.drag(this.nextX, this.nextY, this.xOff, this.yOff);
    }
    @Override
    public void undoTransaction() {
        this.node.drag(this.prevX, this.prevY, 0, 0);
    }
}
