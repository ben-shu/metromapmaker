package metMap.data;

/**
 *
 * @author McKillaGorilla
 */
public interface jTPS_Transaction {
    public void doTransaction();
    public void undoTransaction();
}
