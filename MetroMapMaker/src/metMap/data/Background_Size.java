package metMap.data;

import javafx.scene.layout.Pane;

/**
 *
 * @author Benjamin Shu
 */
public class Background_Size implements jTPS_Transaction {
    private metMapData dataManager;
    private Pane canvas;
    private double prevHeight, prevWidth, nextHeight, nextWidth;
    public Background_Size(metMapData dataManager, Pane canvas, double nextHeight, double nextWidth) {
        this.dataManager = dataManager;
        this.canvas = canvas;
        this.prevHeight = this.canvas.getHeight();
        this.prevWidth = this.canvas.getWidth();
        this.nextHeight = nextHeight;
        this.nextWidth = nextWidth;
    }
    @Override
    public void doTransaction() {
        this.canvas.setMaxHeight(this.nextHeight); this.canvas.setMinHeight(this.nextHeight);
        this.canvas.setMaxWidth(this.nextWidth); this.canvas.setMinWidth(this.nextWidth);
        this.dataManager.setCanvasHeight(this.nextHeight);
        this.dataManager.setCanvasWidth(this.nextWidth);
    }
    @Override
    public void undoTransaction() {
        this.canvas.setMaxHeight(this.prevHeight); this.canvas.setMinHeight(this.prevHeight);
        this.canvas.setMaxWidth(this.prevWidth); this.canvas.setMinWidth(this.prevWidth);
        this.dataManager.setCanvasHeight(this.prevHeight);
        this.dataManager.setCanvasWidth(this.prevWidth);
    }
}
