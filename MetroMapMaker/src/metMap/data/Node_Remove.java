package metMap.data;

import static metMap.data.metMapState.*;

/**
 * Transaction class designed to enable user removal of specified map elements.
 * 
 * @author Benjamin Shu
 */
public class Node_Remove implements jTPS_Transaction {
    private metMapData dataManager;
    private Draggable node;
    private int index;
    
    public Node_Remove(metMapData dataManager, Draggable node) {
        this.dataManager = dataManager;
        this.node = node;
        this.index = this.dataManager.getElements().indexOf(this.node);
    }
    @Override
    public void doTransaction() {
        if (this.node instanceof MetroLine) {
            MetroLine line = (MetroLine) this.node;
            this.dataManager.removeShape(line);
            this.dataManager.removeShape(line.getStartHighlight());
            this.dataManager.removeShape(line.getEndHighlight());
            this.dataManager.removeShape(line.getEndLabel());
            this.dataManager.removeShape(line.getStartLabel());
            for (MetroStation station : line.getStations()) {
                station.removeFromLine(line);
            }
        } else if (this.node instanceof MetroStation) {
            MetroStation station = (MetroStation) this.node;
            this.dataManager.removeShape(station);
            this.dataManager.removeShape(station.getLabel());
            for (MetroLine line : station.getLines()) {
                line.removeStation(station);
            }
        } else if (this.node instanceof MetroLabel) {
            MetroLabel label = (MetroLabel) this.node;
            this.dataManager.removeShape(label);
        } else if (this.node instanceof MetroImage) {
            MetroImage image = (MetroImage) this.node;
            this.dataManager.removeShape(image);
        }
        this.dataManager.deselect();
        this.dataManager.setState(SELECTED_NOTHING);
    }
    @Override
    public void undoTransaction() {
        if (this.node instanceof MetroLine) {
            MetroLine line = (MetroLine) this.node;
            this.dataManager.addShape(this.index, line.getEndLabel());
            this.dataManager.addShape(this.index, line.getStartLabel());
            this.dataManager.addShape(this.index, line.getEndHighlight());
            this.dataManager.addShape(this.index, line.getStartHighlight());
            this.dataManager.addShape(this.index, line);
            for (MetroStation station : line.getStations()) {
                station.addToLine(line);
            }
            this.dataManager.selectShape(line);
        } else if (this.node instanceof MetroStation) {
            MetroStation station = (MetroStation) this.node;
            this.dataManager.addShape(this.index, station.getLabel());
            this.dataManager.addShape(this.index, station);
            this.dataManager.selectShape(station);
            for (MetroLine line : station.getLines()) {
                line.addStation(station);
            }
        } else if (this.node instanceof MetroLabel) {
            MetroLabel label = (MetroLabel) this.node;
            this.dataManager.addShape(this.index, label);
            this.dataManager.selectShape(label);
        } else if (this.node instanceof MetroImage) {
            MetroImage image = (MetroImage) this.node;
            this.dataManager.addShape(this.index, image);
            this.dataManager.selectShape(image);
        }
        this.dataManager.setState(SELECTED_ELEMENT);
    }
}
