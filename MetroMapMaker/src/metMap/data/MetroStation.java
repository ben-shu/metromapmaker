package metMap.data;

import javafx.scene.shape.Circle;
import javafx.scene.paint.Color;
import java.util.ArrayList;

/**
 * Object that defines a metro station as a circle with a label that can be
 * placed at different positions and rotated in different orientations.
 * 
 * @author Benjamin Shu
 */
public class MetroStation extends Circle implements Draggable {
    protected String name;
    protected MetroLabel label;
    protected int labelOrient;
    protected ArrayList<MetroLine> lines;
    protected boolean rotated;
    
    private static final int BOTTOM = 0, LEFT = 1, TOP = 2, RIGHT = 3;
    public MetroStation(String name, Color color) {
        super();        
        this.name = name;
        this.label = new MetroLabel(this.name);
        this.label.setHasParent(true);
        this.label.setFill(Color.BLACK);
        this.label.setStroke(Color.TRANSPARENT);
        this.labelOrient = BOTTOM;
        this.labelBind();
        this.lines = new ArrayList<MetroLine>();
        this.rotated = false;
        this.setFill(color);
        this.setStroke(Color.BLACK);
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
        this.label.setText(this.name);
        this.labelBind();
    }
    public Color getColor() {
        return (Color) this.getFill();
    }
    public void setColor(Color color) {
        this.setFill(color);
    }
    public void adjustRadius(double radius) {
        if (radius >= 5.0 && radius <= 10.0) {
            this.setRadius(radius);
        }
    }
    public ArrayList<MetroLine> getLines() {
        return this.lines;
    }
    public MetroLabel getLabel() {
        return this.label;
    }
    public int getLabelOrient() {
        return this.labelOrient;
    }
    public void setLabelOrient(int labelOrient) {
        if (labelOrient > -1 && labelOrient < 5) {
            this.labelOrient = labelOrient;
        }
    }
    public void placeLabelDo() {
        switch (this.labelOrient) {
            case BOTTOM:
                this.labelOrient = LEFT;
                labelBind();
                break;
            case LEFT:
                this.labelOrient = TOP;
                labelBind();
                break;
            case TOP:
                this.labelOrient = RIGHT;
                labelBind();
                break;
            case RIGHT:
                this.labelOrient = BOTTOM;
                labelBind();
                break;
            default:
                break;
        }
    }
    public void placeLabelUndo() {
        switch (this.labelOrient) {
            case BOTTOM:
                this.labelOrient = RIGHT;
                labelBind();
                break;
            case LEFT:
                this.labelOrient = BOTTOM;
                labelBind();
                break;
            case TOP:
                this.labelOrient = LEFT;
                labelBind();
                break;
            case RIGHT:
                this.labelOrient = TOP;
                labelBind();
                break;
            default:
                break;
        }
    }
    public boolean rotated() {
        return this.rotated;
    }
    public void toggleRotate() {
        if (this.rotated) {
            this.label.rotateProperty().set(0);
        } else {
            this.label.rotateProperty().set(90);
        }
        this.rotated = !this.rotated;
        labelBind();
    }
    public void labelBind() {
        switch (this.labelOrient) {
            case BOTTOM:
                    this.label.xProperty().bind(this.centerXProperty()
                            .subtract(this.label.getLayoutBounds().getWidth() / 2));
                    this.label.yProperty().bind(this.centerYProperty()
                            .add(this.radiusProperty())
                            .add(this.label.getLayoutBounds().getHeight()));
                break;
            case LEFT:
                    this.label.xProperty().bind(this.centerXProperty()
                            .subtract(this.radiusProperty())
                            .subtract(this.label.getLayoutBounds().getWidth())
                            .subtract(5));
                    this.label.yProperty().bind(this.centerYProperty()
                            .add(this.label.getLayoutBounds().getHeight() / 2));
                break;
            case TOP:
                this.label.xProperty().bind(this.centerXProperty()
                        .subtract(this.label.getLayoutBounds().getWidth() / 2));
                this.label.yProperty().bind(this.centerYProperty()
                        .subtract(this.radiusProperty())
                        .subtract(5));
                break;
            case RIGHT:
                this.label.xProperty().bind(this.centerXProperty()
                        .add(this.radiusProperty())
                        .add(5));
                this.label.yProperty().bind(this.centerYProperty()
                        .add(this.label.getLayoutBounds().getHeight() / 2));
                break;
            default:
                break;
        }    
    }
    public void addToLine(MetroLine line) {
        if (!this.lines.contains(line)) {
            this.lines.add(line);
        }
    }
    public void removeFromLine(MetroLine line) {
        this.lines.remove(line);
    }
    public void removeFromAllLines() {
        this.lines.clear();
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MetroStation)) {
            return false;
        }
        MetroStation that = (MetroStation) obj;
        return (this.getName().equals(that.getName())) &&
                (this.getLines().equals(that.getLines())) &&
                (this.getFill().equals(that.getColor())) &&
                (this.getX() == that.getX()) && (this.getY() == that.getY()) &&
                (this.getRadius() == that.getRadius());
    }
    
    /**
     * Methods implemented from Draggable. 
     */
    @Override
    public double getX() {
        return this.getCenterX();
    }
    @Override
    public void setX(double x) {
        this.setCenterX(x);
    }
    @Override
    public double getY() {
        return this.getCenterY();
    }
    @Override
    public void setY(double y) {
        this.setCenterY(y);
    }
    @Override
    public void drag(double x, double y, double xOff, double yOff) {
        this.setCenterX(x + xOff); this.setCenterY(y + yOff);
    }
    @Override
    public String getType() {
        return Draggable.STATION;
    }
    public double getHeight() {
        return this.getRadius() * 2;
    }
    public double getWidth() {
        return this.getRadius() * 2;
    }
    
    @Override
    public String toString() {
        return this.name;
    }
}
