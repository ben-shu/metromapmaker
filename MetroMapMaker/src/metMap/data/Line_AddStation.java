package metMap.data;

/**
 *
 * @author Benjamin Shu
 */
public class Line_AddStation implements jTPS_Transaction {
private metMapData dataManager;
    private MetroLine line;
    private MetroStation station;
    
    public Line_AddStation(metMapData dataManager, MetroLine line, MetroStation station) {
        this.dataManager = dataManager;
        this.line = line;
        this.station = station;
    }
    @Override
    public void doTransaction() {
        this.line.addStation(this.station);
        this.dataManager.moveStationToLine(this.station, this.line);
    }
    @Override
    public void undoTransaction() {
        this.line.removeStation(this.station);
    }
}
