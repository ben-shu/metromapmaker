package metMap.data;

import javafx.scene.paint.Paint;

/**
 * Transaction class designed to enable changing the background of the 
 * Metro Map Maker workspace.
 * 
 * @author Benjamin Shu
 */
public class Background_Fill implements jTPS_Transaction {
    // BackgroundFill objects stored to keep transaction simple
    private Paint prevFill, nextFill;
    private metMapData dataManager;
    
    public Background_Fill(metMapData dataManager, Paint nextFill) {
        this.dataManager = dataManager;
        this.prevFill = this.dataManager.getBackground();
        this.nextFill = nextFill;
    }
    @Override
    public void doTransaction() {
        this.dataManager.setBackground(this.nextFill);
    }
    @Override
    public void undoTransaction() {
        this.dataManager.setBackground(this.prevFill);
    }
}
