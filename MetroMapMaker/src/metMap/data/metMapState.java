package metMap.data;

/**
 * Enum class used to track the possible states of metMapData.
 * 
 * @author Benjamin Shu
 */
public enum metMapState {
    SELECTED_NOTHING,
    SELECTED_ELEMENT,
    SELECTED_START,
    SELECTED_END,
    DRAGGING_ELEMENT,
    DRAGGING_START,
    DRAGGING_END,
    ADDING_STATION,
    REMOVING_STATION;
}
