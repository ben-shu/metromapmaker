package metMap.data;

import javafx.scene.paint.Color;

/**
 * Transaction class designed to enable user modification of selected map 
 * elements.
 * @author Benjamin Shu
 */
public class Node_Edit implements jTPS_Transaction {
    // Stored copies of element both before and after change.
    private Draggable node;
    private String prevName, nextName;
    private Color prevColor, nextColor;
    private boolean prevCirc, nextCirc, placeLabel, rotateLabel;
    
    public Node_Edit(Draggable node, Color nextColor, String nextName) {
        if (node instanceof MetroStation) {
            this.node = node;
            MetroStation station = (MetroStation) this.node;
            this.prevName = station.getName();
            this.nextName = nextName;
            this.prevColor = station.getColor();
            this.nextColor = nextColor;
        }
    }
    public Node_Edit(MetroStation node, boolean placeLabel, boolean rotateLabel) {
        this.node = node;
        this.prevName = node.getName();
        this.nextName = this.prevName;
        this.prevColor = node.getColor();
        this.nextColor = this.prevColor;
        this.placeLabel = placeLabel;
        this.rotateLabel = rotateLabel;
    }
    public Node_Edit(Draggable node, Color nextColor, String nextName, boolean nextCirc) {
        if (node instanceof MetroLine) {
            this.node = node;
            MetroLine line = (MetroLine) this.node;
            this.prevName = line.getName();
            this.nextName = nextName;
            this.prevColor = line.getColor();
            this.nextColor = nextColor;
            this.prevCirc = line.isCircular();
            this.nextCirc = nextCirc;
        }
    }
    public Node_Edit(Draggable node, boolean nextCirc) {
        if (node instanceof MetroLine) {
            this.node = node;
            MetroLine line = (MetroLine) this.node;
            this.prevName = line.getName();
            this.nextName = this.prevName;
            this.prevColor = line.getColor();
            this.nextColor = this.prevColor;
            this.prevCirc = line.isCircular();
            this.nextCirc = nextCirc;
        }
    }
    public Node_Edit(Draggable node, double nextWidth) {
        if (node instanceof MetroLine) {
            this.node = node;
            MetroLine line = (MetroLine) this.node;
            this.prevName = line.getName();
            this.nextName = this.prevName;
            this.prevColor = line.getColor();
            this.nextColor = this.prevColor;
            this.prevCirc = line.isCircular();
            this.nextCirc = this.prevCirc;
        } else if (node instanceof MetroStation) {
            this.node = node;
            MetroStation station = (MetroStation) this.node;
            this.prevName = station.getName();
            this.nextName = this.prevName;
            this.prevColor = station.getColor();
            this.nextColor = this.prevColor;
        }
    }
    @Override
    public void doTransaction() {
        if (this.node instanceof MetroLine) {
            MetroLine line = (MetroLine) this.node;
            line.setName(this.nextName);
            line.setColor(this.nextColor);
            if (this.prevCirc != this.nextCirc) { line.toggleCircular(); }
        } else if (this.node instanceof MetroStation) {
            MetroStation station = (MetroStation) this.node;
            station.setName(this.nextName);
            station.setColor(this.nextColor);
            if (this.placeLabel) { station.placeLabelDo(); }
            if (this.rotateLabel) { station.toggleRotate(); }
        }
    }
    @Override
    public void undoTransaction() {
        if (this.node instanceof MetroLine) {
            MetroLine line = (MetroLine) this.node;
            line.setName(this.prevName);
            line.setColor(this.prevColor);
            if (this.prevCirc != this.nextCirc) { line.toggleCircular(); }
        } else if (this.node instanceof MetroStation) {
            MetroStation station = (MetroStation) this.node;
            station.setName(this.prevName);
            station.setColor(this.prevColor);
            if (this.placeLabel) { station.placeLabelUndo(); }
            if (this.rotateLabel) { station.toggleRotate(); }
        }
    }
}
