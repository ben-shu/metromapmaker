package metMap.data;

import javafx.scene.shape.Polyline;
import javafx.scene.shape.Circle;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 * Subclass of Line designed to emulate a subway line in Metro Map Maker.
 * 
 * @author Benjamin Shu
 */
public class MetroLine extends Polyline implements Draggable {
    protected String name;                      // Save for HW 5
    protected MetroLabel startLabel, endLabel;
    protected final MetroLineEnd startHighlight, endHighlight;
    protected ArrayList<MetroStation> stations; // Save for HW 5
    protected ArrayList<PointDoubleProperty> positions;
    protected boolean circular;                 // Save for HW 5
    protected boolean bold, italic;
    
    public static final String START = "start", END = "end";
    
    /**
     * Class-specific constructors and methods.
     */
    public MetroLine(String name, Color color) {
        super();
        this.name = name;
        
        this.stations = new ArrayList<>();
        this.positions = new ArrayList<>();
        
        this.getPoints().add(0.0);
        this.positions.add(new PointDoubleProperty(this.getPoints(), 0, 0.0));
        this.positions.get(0).enableListener();
        this.getPoints().add(0.0);
        this.positions.add(new PointDoubleProperty(this.getPoints(), 1, 0.0));
        this.positions.get(1).enableListener();
        this.startLabel = new MetroLabel(this.name);
        this.startLabel.setHasParent(true);
        this.startLabel.xProperty().bind(this.positions.get(0)
                .subtract(this.startLabel.getLayoutBounds().getWidth())
                .subtract(20));
        this.startLabel.yProperty().bind(this.positions.get(1)
                .add(this.startLabel.getLayoutBounds().getHeight() / 2));
        
        this.getPoints().add(0.0);
        this.positions.add(new PointDoubleProperty(this.getPoints(), 2, 0.0));
        this.positions.get(2).enableListener();
        this.getPoints().add(0.0);
        this.positions.add(new PointDoubleProperty(this.getPoints(), 3, 0.0));
        this.positions.get(3).enableListener();
        
        this.endLabel = new MetroLabel(this.name);
        this.endLabel.setHasParent(true);
        this.endLabel.xProperty().bind(this.positions.get(2)
                .add(20));
        this.endLabel.yProperty().bind(this.positions.get(3)
                .add(this.endLabel.getLayoutBounds().getHeight() / 2));
        
        this.startHighlight = new MetroLineEnd(this, START);
        this.endHighlight = new MetroLineEnd(this, END);
        
        this.circular = false;
        this.bold = false;
        this.italic = false;
        this.setFill(Color.TRANSPARENT);
        this.setStroke(color);
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
        this.startLabel.setText(this.name);
        this.startLabel.xProperty().bind(this.positions.get(0)
                .subtract(this.startLabel.getLayoutBounds().getWidth())
                .subtract(20));
        this.startLabel.yProperty().bind(this.positions.get(1)
                .add(this.startLabel.getLayoutBounds().getHeight() / 2));
        this.endLabel.setText(this.name);
        this.endLabel.xProperty().bind(this.positions.get(this.positions.size() - 2)
                .add(20));
        this.endLabel.yProperty().bind(this.positions.get(this.positions.size() - 1)
                .add(this.endLabel.getLayoutBounds().getHeight() / 2));
    }
    public Color getColor() {
        return (Color) this.getStroke();
    }
    public void setColor(Color color) {
        this.setStroke(color);
        this.setFill(Color.TRANSPARENT);
    }
    public void adjustWidth(double width) {
        if (width >= 5 && width <= 10) {
            this.setStrokeWidth(width);
        }
    }
    public boolean isCircular() {
        return this.circular;
    }
    public void toggleCircular() {
        if (this.circular) {
            int size = this.positions.size();
            
            this.positions.get(0).unbindBidirectional(this.positions.get(size - 4));
            this.positions.get(1).unbindBidirectional(this.positions.get(size - 3));
            
            this.positions.get(size - 2).unbindBidirectional(this.positions.get(2));
            this.positions.get(size - 1).unbindBidirectional(this.positions.get(3));
            
            this.startLabel.setText(this.name); this.endLabel.setText(this.name);
            this.circular = false;
        } else {
            if (this.stations.size() >= 3) {
                int size = this.positions.size();
                this.positions.get(0).bindBidirectional(this.positions.get(size - 4));
                this.positions.get(1).bindBidirectional(this.positions.get(size - 3));
                
                this.positions.get(size - 2).bindBidirectional(this.positions.get(2));
                this.positions.get(size - 1).bindBidirectional(this.positions.get(3));
                
                this.startLabel.setText(""); this.endLabel.setText("");
                this.circular = true;
            }
        }
    }
    
    public ArrayList<MetroStation> getStations() {
        return this.stations;
    }
    public void addStationFromList(MetroStation station) {
        int size = this.positions.size(),
                x = size - 2, y = size - 1;
        this.positions.add(x, new PointDoubleProperty(this.getPoints(), x, station.getCenterX()));
        this.positions.get(x).bindBidirectional(station.centerXProperty());
        this.positions.get(x).enableListener();
        this.getPoints().add(x, station.getCenterX());
        
        this.positions.add(y, new PointDoubleProperty(this.getPoints(), y, station.getCenterY()));
        this.positions.get(y).bindBidirectional(station.centerYProperty());
        this.positions.get(y).enableListener();
        this.getPoints().add(y, station.getCenterY());
        
        size = this.positions.size();
        this.positions.get(size - 2).updateListenerIndex(size - 2);
        this.positions.get(size - 1).updateListenerIndex(size - 1);
        
        this.stations.add(station);
        station.addToLine(this);
    }
    public void addStation(MetroStation station) {
        if (!(this.stations.contains(station))) {
            double xOff1 = station.getCenterX() - this.positions.get(0).doubleValue(),
                    yOff1 = station.getCenterY() - this.positions.get(1).doubleValue(),
                    disp = Math.sqrt((xOff1 * xOff1) + (yOff1 * yOff1)),
                    minDisp = disp;
            int index = 2;
            int size = this.positions.size();
            for (int i = 2; i < size - 3; i += 2) {
                xOff1 = station.getCenterX() - this.positions.get(i).doubleValue();
                yOff1 = station.getCenterY() - this.positions.get(i + 1).doubleValue();
                disp = Math.sqrt((xOff1 * xOff1) + (yOff1 * yOff1));
                if (disp < minDisp) {
                    minDisp = disp;
                    index = i + 2;
                }
            }
            final int bindX = index;
            this.positions.add(bindX, new PointDoubleProperty(this.getPoints(),
                    bindX, station.getCenterX()));
            this.positions.get(bindX).bindBidirectional(station.centerXProperty());
            this.positions.get(bindX).enableListener();
            this.getPoints().add(bindX, station.getCenterX());
            
            final int bindY = index + 1;
            this.positions.add(bindY, new PointDoubleProperty(this.getPoints(),
                    bindY, station.getCenterX()));
            this.positions.get(bindY).bindBidirectional(station.centerYProperty());
            this.positions.get(bindY).enableListener();
            this.getPoints().add(bindY, station.getCenterY());
            
            this.stations.add((bindX / 2) - 1, station);
            station.addToLine(this);
            size = this.positions.size();
            for (int i = bindX + 2; i < size - 3; i += 2) {
                this.positions.get(i).unbindBidirectional(
                        this.stations.get((i / 2) - 2).centerXProperty());
                this.positions.get(i).bindBidirectional(
                        this.stations.get((i / 2) - 1).centerXProperty());
                this.positions.get(i).updateListenerIndex(i);
                
                this.positions.get(i + 1).unbindBidirectional(
                        this.stations.get((i / 2) - 2).centerYProperty());
                this.positions.get(i + 1).bindBidirectional(
                        this.stations.get((i / 2) - 1).centerYProperty());
                this.positions.get(i + 1).updateListenerIndex(i + 1);
            }
            this.positions.get(size - 2).updateListenerIndex(size - 2);
            this.positions.get(size - 1).updateListenerIndex(size - 1);
        }
    }
    public void removeStation(MetroStation station) {
        if (this.stations.contains(station)) {
            int index = (this.stations.indexOf(station) + 1) * 2;
            this.positions.get(index).unbindBidirectional(station.centerXProperty());
            this.positions.get(index).disableListener();
            this.positions.get(index + 1).unbindBidirectional(station.centerYProperty());
            this.positions.get(index + 1).disableListener();
            
            this.positions.remove(index);
            this.positions.remove(index);
            this.getPoints().remove(index);
            this.getPoints().remove(index);
            this.stations.remove(station);
            
            int size = this.positions.size();
            for (int i = index; i < size - 3; i += 2) {
                this.positions.get(i).updateListenerIndex(i);
                this.positions.get(i + 1).updateListenerIndex(i + 1);
            }
            this.positions.get(size - 2).updateListenerIndex(size - 2);
            this.positions.get(size - 1).updateListenerIndex(size - 1);
        }
    }
    public MetroStation getStation(String name) {
        for (MetroStation station : this.stations) {
            if (station.getName().equals(name)) {
                return station;
            }
        }
        return null;
    }
    public boolean hasStation(MetroStation station) {
        return this.stations.contains(station);
    }
    
    public ArrayList<PointDoubleProperty> getPositions() {
        return this.positions;
    }
    public double getStartX() {
        return this.positions.get(0).doubleValue();
    }
    public double getStartY() {
        return this.positions.get(1).doubleValue();
    }
    public Circle getStartHighlight() {
        return this.startHighlight;
    }
    public void addStartHighlight() {
        this.startHighlight.setFill(this.getColor());
        this.startHighlight.setRadius(this.getStrokeWidth());
    }
    public void removeStartHighlight() {
        this.startHighlight.setFill(Color.TRANSPARENT);
        this.startHighlight.setRadius(0.0);
    }
    public void setStart(double x, double y) {
        this.positions.get(0).set(x); this.positions.get(1).set(y);
    }
    public boolean startContains(double x, double y) {
        double xOff = x - this.positions.get(0).doubleValue(),
                yOff = y - this.positions.get(1).doubleValue();
        return Math.sqrt((xOff * xOff) + (yOff * yOff)) <= this.getStrokeWidth();
    }
    public void dragStart(double x, double y, double xOff, double yOff) {
        this.setStart(x + xOff, y + yOff);
    }
    public MetroLabel getStartLabel() {
        return this.startLabel;
    }
    public double getEndX() {
        return this.positions.get(this.positions.size() - 2).doubleValue();
    }
    public double getEndY() {
        return this.positions.get(this.positions.size() - 1).doubleValue();
    }
    public Circle getEndHighlight() {
        return this.endHighlight;
    }
    public void addEndHighlight() {
        this.endHighlight.setFill(this.getColor());
        this.endHighlight.setRadius(this.getStrokeWidth());
    }
    public void removeEndHighlight() {
        this.endHighlight.setFill(Color.TRANSPARENT);
        this.endHighlight.setRadius(0.0);
    }
    public void setEnd(double x, double y) {
        int size = this.positions.size();
        this.positions.get(size - 2).set(x);
        this.positions.get(size - 1).set(y);
    }
    public boolean endContains(double x, double y) {
        int size = this.positions.size();
        double xOff = x - this.positions.get(size - 2).doubleValue(), 
                yOff = y - this.positions.get(size - 1).doubleValue();
        return Math.sqrt((xOff * xOff) + (yOff * yOff)) <= this.getStrokeWidth();
    }
    public void dragEnd(double x, double y, double xOff, double yOff) {
        this.setEnd(x + xOff, y + yOff);
    }
    public MetroLabel getEndLabel() {
        return this.endLabel;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MetroLine)) {
            return false;
        }
        MetroLine that = (MetroLine) obj;
        return (this.getName().equals(that.getName())) &&
                (this.getStations().equals(that.getStations())) &&
                (this.getColor().equals(that.getColor())) &&
                (this.getX() == that.getX()) && 
                (this.getY() == that.getY()) &&
                (this.getStrokeWidth() == that.getStrokeWidth());
    }
    
    /** 
     * Methods implemented from Draggable.
     */
    @Override
    public double getX() {
        int size = this.positions.size();
        double startX = this.positions.get(0).doubleValue(),
                endX = this.positions.get(size - 2).doubleValue();
        return (startX + endX) / 2;
    }
    @Override
    public void setX(double x) {
        double xOff = x - this.getX();
        int size = this.positions.size();
        for (int i = (this.circular ? 2 : 0); i < (this.circular ? size - 2 : size); i += 2) {
            this.positions.get(i).set(this.positions.get(i).doubleValue()+ xOff);
        }
    }
    @Override
    public double getY() {
        int size = this.positions.size();
        double startY = this.positions.get(1).doubleValue(),
                endY = this.positions.get(size - 1).doubleValue();
        return (startY + endY) / 2;
    }
    @Override
    public void setY(double y) {
        double yOff = y - this.getY();
        int size = this.positions.size();
        for (int i = (this.circular ? 3 : 1); i < (this.circular ? size - 1 : size); i += 2) {
            this.positions.get(i).set(this.positions.get(i).doubleValue()+ yOff);
        }
    }
    public double getHeight() {
        int size = this.positions.size();
        return this.positions.get(size - 2).doubleValue() - this.positions.get(1).doubleValue();
    }
    public double getWidth() {
        int size = this.positions.size();
        return this.positions.get(size - 1).doubleValue() - this.positions.get(0).doubleValue();
    }
    @Override
    public void drag(double x, double y, double xOff, double yOff) {
        this.setX(x + xOff); this.setY(y + yOff);
    }
    @Override
    public String getType() {
        return Draggable.LINE;
    }
    
    @Override
    public String toString() {
        return this.name;
    }
    
    class PointDoubleProperty extends SimpleDoubleProperty {
        private ChangeListener pointListener;
        private boolean enabled;
        private final ObservableList<Double> points;
        private PointDoubleProperty(ObservableList<Double> points, int index, double point) {
            super(point);
            this.points = points;
            this.enabled = false;
            final int i = index;
            this.pointListener = (ChangeListener) 
                (ObservableValue obs, Object oldVal, Object newVal) -> {
                    this.points.set(i, this.doubleValue());
                };
        }
        protected void updateListenerIndex(int index) {
            final int i = index;
            if (this.enabled) { this.removeListener(this.pointListener); }
            this.pointListener = (ChangeListener)
                (ObservableValue obs, Object oldVal, Object newVal) -> {
                    this.points.set(i, this.doubleValue());
                };
            if (this.enabled) { this.addListener(this.pointListener); }
        }
        protected void enableListener() {
            if (!this.enabled) {
                this.addListener(this.pointListener);
                this.enabled = true;
            }
        }
        protected void disableListener() {
            if (this.enabled) {
                this.removeListener(this.pointListener);
                this.enabled = false;
            }
        }
    }
    public class MetroLineEnd extends Circle implements Draggable {
        private final String TYPE;
        private final MetroLine PARENT;
        protected MetroLineEnd(MetroLine line, String type) {
            super();
            this.TYPE = type;
            this.PARENT = line;
            this.setRadius(0.0);
            this.setFill(Color.TRANSPARENT);
            if (this.TYPE.equals(START)) {
                this.setCenterX(this.PARENT.getPositions().get(0).doubleValue());
                this.centerXProperty().bindBidirectional(this.PARENT.getPositions().get(0));
                this.setCenterY(this.PARENT.getPositions().get(1).doubleValue());
                this.centerYProperty().bindBidirectional(this.PARENT.getPositions().get(1));
            } else {
                int size = this.PARENT.getPositions().size();
                this.setCenterX(this.PARENT.getPositions().get(size - 2).doubleValue());
                this.centerXProperty().bindBidirectional(this.PARENT.getPositions().get(size - 2));
                this.setCenterY(this.PARENT.getPositions().get(size - 1).doubleValue());
                this.centerYProperty().bindBidirectional(this.PARENT.getPositions().get(size - 1));
            }
        }
        public MetroLine getLine() {
            return this.PARENT;
        }
        public String getEndType() {
            return this.TYPE;
        }
        @Override
        public double getX() {
            return this.getCenterX();
        }
        @Override
        public void setX(double x) {
            this.setCenterX(x);
        }
        @Override
        public double getY() {
            return this.getCenterY();
        }
        @Override
        public void setY(double y) {
            this.setCenterY(y);
        }
        @Override
        public void drag(double x, double y, double xOff, double yOff) {
            this.setCenterX(x + xOff); this.setCenterY(y + yOff);
        }
        @Override
        public String getType() {
            return LINE_END;
        }
    }
}