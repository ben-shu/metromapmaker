package metMap.data;

import javafx.scene.text.Text;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;

/**
 *
 * @author Benjamin Shu
 */
public class MetroLabel extends Text implements Draggable, TextEditable {
    private boolean bold, italic, hasParent;
    
    public MetroLabel(String text) {
        super(text);
        this.bold = false;
        this.italic = false;
        this.hasParent = false;
    }
    public double getHeight() {
        return this.getLayoutBounds().getHeight();
    }
    public double getWidth() {
        return this.getLayoutBounds().getHeight();
    }
    public boolean hasParent() {
        return this.hasParent;
    }
    protected void setHasParent(boolean hasParent) {
        this.hasParent = hasParent;
    }
    @Override
    public void drag(double x, double y, double xOff, double yOff) {
        this.setX(x + xOff); this.setY(y + yOff);
    }
    @Override
    public String getType() {
        return Draggable.LABEL;
    }

    /**
     * Methods implemented from TextEditable
     */
    @Override
    public Color getTextFill() {
        return (Color) this.getFill();
    }
    @Override
    public void setTextFill(Color color) {
        this.setFill(color);
    }
    @Override
    public String getFamily() {
        return this.getFont().getFamily();
    }
    @Override
    public void setFamily(String family) {
        this.setFont(Font.font(family,
                (this.bold ? FontWeight.BOLD : FontWeight.NORMAL),
                (this.italic ? FontPosture.ITALIC : FontPosture.REGULAR),
                this.getFont().getSize()));
    }
    @Override
    public boolean isBold() {
        return this.bold;
    }
    @Override
    public void toggleBold() {
        this.bold = !this.bold;
        Font labelFont = this.getFont();
        this.setFont(Font.font(labelFont.getFamily(),
                (this.bold ? FontWeight.BOLD : FontWeight.NORMAL),
                (this.italic ? FontPosture.ITALIC : FontPosture.REGULAR),
                labelFont.getSize()));
    }
    @Override
    public boolean isItalic() {
        return this.italic;
    }
    @Override
    public void toggleItalic() {
        this.italic = !this.italic;
        Font labelFont = this.getFont();
        this.setFont(Font.font(labelFont.getFamily(),
                (this.bold ? FontWeight.BOLD : FontWeight.NORMAL),
                (this.italic ? FontPosture.ITALIC : FontPosture.REGULAR),
                labelFont.getSize()));
    }
    @Override
    public double getFontSize() {
        return this.getFont().getSize();
    }
    @Override
    public void setFontSize(double size) {
        this.setFont(Font.font(this.getFont().getFamily(),
                (this.bold ? FontWeight.BOLD : FontWeight.NORMAL),
                (this.italic ? FontPosture.ITALIC : FontPosture.REGULAR),
                size));
    }
}
