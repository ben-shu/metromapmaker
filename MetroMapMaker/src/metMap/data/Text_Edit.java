package metMap.data;

import javafx.scene.paint.Color;

/**
 *
 * @author Benjamin Shu
 */
public class Text_Edit implements jTPS_Transaction {
    private MetroLabel node;
    private boolean bold, italic;
    private Color prevColor, nextColor;
    private double prevSize, nextSize;
    private String prevFamily, nextFamily;
    public Text_Edit(MetroLabel node, boolean bold, boolean italic) {
        this.node = node;
        this.bold = bold;
        this.italic = italic;
        this.prevColor = node.getTextFill();
        this.nextColor = this.prevColor;
        this.prevSize = node.getFontSize();
        this.nextSize = this.prevSize;
        this.prevFamily = node.getFamily();
        this.nextFamily = this.prevFamily;
    }
    public Text_Edit(MetroLabel node, Color nextColor) {
        this.node = node;
        this.prevColor = node.getTextFill();
        this.nextColor = nextColor;
        this.prevSize = node.getFontSize();
        this.nextSize = this.prevSize;
        this.prevFamily = node.getFamily();
        this.nextFamily = this.prevFamily;
    }
    public Text_Edit(MetroLabel node, double nextSize, String nextFamily) {
        this.node = node;
        this.prevColor = node.getTextFill();
        this.nextColor = this.prevColor;
        this.prevSize = node.getFontSize();
        this.nextSize = nextSize;
        this.prevFamily = node.getFamily();
        this.nextFamily = nextFamily;
    }
    @Override
    public void doTransaction() {
        this.node.setTextFill(this.nextColor);
        if (this.bold) { this.node.toggleBold(); }
        if (this.italic) { this.node.toggleItalic(); }
        this.node.setFontSize(this.nextSize);
        this.node.setFamily(this.nextFamily);
    }
    @Override
    public void undoTransaction() {
        this.node.setTextFill(this.prevColor);
        if (this.bold) { this.node.toggleBold(); }
        if (this.italic) { this.node.toggleItalic(); }
        this.node.setFontSize(this.prevSize);
        this.node.setFamily(this.prevFamily);
    }
}
