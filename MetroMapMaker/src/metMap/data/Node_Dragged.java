package metMap.data;

/**
 *
 * @author Benjamin Shu
 */
public class Node_Dragged implements jTPS_Transaction {
    /**
     * Blank transaction used to separate two different drags.
     */
    public Node_Dragged() {  }
    @Override
    public void doTransaction() {
        
    }
    @Override
    public void undoTransaction() {
        
    }
}
