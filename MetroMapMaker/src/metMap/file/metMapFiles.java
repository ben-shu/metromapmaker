package metMap.file;

import djf.AppTemplate;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import metMap.data.metMapData;
import metMap.data.Draggable;
import metMap.data.MetroLine;
import metMap.data.MetroStation;
import metMap.data.MetroImage;
import metMap.data.MetroLabel;
import metMap.gui.metMapWorkspace;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.paint.Paint;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.image.Image;
import javafx.scene.shape.Shape;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

import java.util.ArrayList;

import static djf.settings.AppStartupConstants.*;
import java.io.File;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.geometry.Rectangle2D;
import javax.imageio.ImageIO;

/**
 * File manager class designed to handle file I/O for Metro Map Maker.
 * 
 * @author Benjamin Shu
 */
public class metMapFiles implements AppFileComponent {
    private static final String JSON_NAME = "name",
            JSON_CANVAS_WIDTH = "canvas_width", JSON_CANVAS_HEIGHT = "canvas_height",
            JSON_BACKGROUND = "background",
            BACKGROUND_TYPE = "background_type",
            BACKGROUND_COLOR = "background_color",
            BACKGROUND_IMAGE = "background_image",
            BACKGROUND_VALUE = "background_value",
            JSON_COLOR = "color",
            JSON_RED = "red", JSON_GREEN = "green", JSON_BLUE = "blue", JSON_ALPHA = "alpha",
            JSON_LINES = "lines", JSON_STATIONS = "stations", JSON_ELEMENTS = "elements",
            JSON_TYPE = "type",
            JSON_X = "x", JSON_Y = "y",
            JSON_START_X = "start_x", JSON_START_Y = "start_y",
            JSON_START_LABEL = "start_label",
            JSON_END_X = "end_x", JSON_END_Y = "end_y",
            JSON_END_LABEL = "end_label",
            JSON_CIRCULAR = "circular",
            JSON_STATION_NAMES = "station_names",
            JSON_RADIUS = "radius", JSON_LABELORIENT = "labelOrient", JSON_ROTATED = "rotated",
            JSON_TEXT = "text",
            JSON_FONT_FAMILY = "font_family",
            JSON_FONT_SIZE = "font_size",
            JSON_FONT_BOLD = "font_bold",
            JSON_FONT_ITALIC = "font_italic",
            JSON_FONT_COLOR = "font_color",
            JSON_IMAGE = "image",
            JSON_LABEL = "label",
            JSON_FILL_COLOR = "fill_color",
            JSON_STROKE_COLOR = "stroke_color",
            JSON_STROKE_WIDTH = "stroke_width",
            DEFAULT_DOCTYPE_DECLARATION = "<!doctype html>\n",
            DEFAULT_ATTRIBUTE_VALUE = "";
    
    private AppTemplate app;
    
    private String mapName;
    
    public metMapFiles(AppTemplate app) {
        this.app = app;
    }
    public String getMapName() {
        return this.mapName;
    }
    private double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    private JsonObject makeColorJsonObject(Color color) {
        JsonObject colorJson = Json.createObjectBuilder()
                .add(JSON_RED, color.getRed())
                .add(JSON_GREEN, color.getGreen())
                .add(JSON_BLUE, color.getBlue())
                .add(JSON_ALPHA, color.getOpacity())
                .build();
        return colorJson;
    }
    private Color loadColor(JsonObject json, String colorToGet) {
	JsonObject jsonColor = json.getJsonObject(colorToGet);
	double red = getDataAsDouble(jsonColor, JSON_RED);
	double green = getDataAsDouble(jsonColor, JSON_GREEN);
	double blue = getDataAsDouble(jsonColor, JSON_BLUE);
	double alpha = getDataAsDouble(jsonColor, JSON_ALPHA);
	Color loadedColor = new Color(red, green, blue, alpha);
	return loadedColor;
    }
    private Image loadImage(JsonObject json, String imageToGet) {
        JsonObject imageJson = json.getJsonObject(imageToGet);
        String imageURI = imageJson.getString(JSON_IMAGE);
        Image image = new Image(imageURI);
        return image;
    }
    private MetroLine loadLine(JsonObject lineJson, ArrayList<MetroStation> stations) {
        String name = lineJson.getString(JSON_NAME);
        Color color = loadColor(lineJson, JSON_COLOR);
        MetroLine line = new MetroLine(name, color);
        line.setStrokeWidth(getDataAsDouble(lineJson, JSON_STROKE_WIDTH));
        line.setStart(getDataAsDouble(lineJson, JSON_START_X),
                getDataAsDouble(lineJson, JSON_START_Y));
        line.setEnd(getDataAsDouble(lineJson, JSON_END_X),
                getDataAsDouble(lineJson, JSON_END_Y));
        
        JsonObject labelJson = lineJson.getJsonObject(JSON_START_LABEL);
        MetroLabel label = line.getStartLabel();
        label.setFamily(labelJson.getString(JSON_FONT_FAMILY));
        label.setFontSize(labelJson.getInt(JSON_FONT_SIZE));
        if (labelJson.getBoolean(JSON_FONT_BOLD)) { label.toggleBold(); }
        if (labelJson.getBoolean(JSON_FONT_ITALIC)) { label.toggleItalic(); }
        label.setTextFill(loadColor(labelJson, JSON_FONT_COLOR));
        
        labelJson = lineJson.getJsonObject(JSON_END_LABEL);
        label = line.getEndLabel();
        label.setFamily(labelJson.getString(JSON_FONT_FAMILY));
        label.setFontSize(labelJson.getInt(JSON_FONT_SIZE));
        if (labelJson.getBoolean(JSON_FONT_BOLD)) { label.toggleBold(); }
        if (labelJson.getBoolean(JSON_FONT_ITALIC)) { label.toggleItalic(); }
        label.setTextFill(loadColor(labelJson, JSON_FONT_COLOR));
        
        // Add all stations
        JsonArray lineStations = lineJson.getJsonArray(JSON_STATION_NAMES);
        String stationName; int size = lineStations.size();
        for (int i = 0; i < size; i++) {
            stationName = lineStations.getString(i);
            for (MetroStation station : stations) {
                if (station.getName().equals(stationName)) {
                    line.addStationFromList(station);
                    break;
                }
            }
        }
        boolean circular = lineJson.getBoolean(JSON_CIRCULAR);
        if (circular) { line.toggleCircular(); }
        return line;
    }
    private MetroStation loadStation(JsonObject stationJson) {
        String name = stationJson.getString(JSON_NAME);
        Color color = loadColor(stationJson, JSON_COLOR);
        MetroStation station = new MetroStation(name, color);
        station.setX(getDataAsDouble(stationJson, JSON_X));
        station.setY(getDataAsDouble(stationJson, JSON_Y));
        station.setRadius(getDataAsDouble(stationJson, JSON_RADIUS));
        
        station.setLabelOrient(stationJson.getInt(JSON_LABELORIENT));
        station.labelBind();
        if (stationJson.getBoolean(JSON_ROTATED)) { station.toggleRotate(); }
        
        MetroLabel label = station.getLabel();
        label.setFamily(stationJson.getString(JSON_FONT_FAMILY));
        label.setFontSize(stationJson.getInt(JSON_FONT_SIZE));
        if (stationJson.getBoolean(JSON_FONT_BOLD)) { label.toggleBold(); }
        if (stationJson.getBoolean(JSON_FONT_ITALIC)) { label.toggleItalic(); }
        label.setTextFill(loadColor(stationJson, JSON_FONT_COLOR));
        
        return station;
    }
    private MetroImage loadMetroImage(JsonObject imageJson) {
        MetroImage image = new MetroImage(imageJson.getString(JSON_IMAGE));
        image.setX(getDataAsDouble(imageJson, JSON_X));
        image.setY(getDataAsDouble(imageJson, JSON_Y));
        return image;
    }
    private MetroLabel loadMetroLabel(JsonObject labelJson) {
        MetroLabel label = new MetroLabel(labelJson.getString(JSON_TEXT));
        label.setX(getDataAsDouble(labelJson, JSON_X));
        label.setY(getDataAsDouble(labelJson, JSON_Y));
        label.setFamily(labelJson.getString(JSON_FONT_FAMILY));
        label.setFontSize(getDataAsDouble(labelJson, JSON_FONT_SIZE));
        if (labelJson.getBoolean(JSON_FONT_BOLD)) { label.toggleBold(); }
        if (labelJson.getBoolean(JSON_FONT_ITALIC)) { label.toggleItalic(); }
        label.setTextFill(loadColor(labelJson, JSON_FONT_COLOR));
        return label;
    }
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        // Get data manager for Metro Map Maker
        metMapData dataManager = (metMapData) data;
        // Make JSON builder for whole file
        JsonObjectBuilder dataJson = Json.createObjectBuilder();
        
        String[] pathPieces = filePath.split("\\\\");
        this.mapName = pathPieces[pathPieces.length - 1];
        dataJson.add(JSON_NAME, this.mapName);
        
        dataJson.add(JSON_CANVAS_HEIGHT, dataManager.getCanvasHeight());
        dataJson.add(JSON_CANVAS_WIDTH, dataManager.getCanvasWidth());
        
        // Get background of current map
        Paint background = dataManager.getBackground();
        // Create JSON for background
        JsonObjectBuilder backgroundBuilder = Json.createObjectBuilder();
        if (background instanceof Color) {
            // Specify type of background
            backgroundBuilder.add(BACKGROUND_TYPE, BACKGROUND_COLOR);
            // Load background color into JSON object based on RGB values
            Color bgColor = (Color)background;
            JsonObject backgroundValueJson = makeColorJsonObject(bgColor);
            // Add background color to background JSON builder
            backgroundBuilder.add(BACKGROUND_VALUE, backgroundValueJson);
        } else if (background instanceof ImagePattern) {
            // Specify type of background
            backgroundBuilder.add(BACKGROUND_TYPE, BACKGROUND_IMAGE);
            // Load background image's URI into JSON object
            JsonObject backgroundValueJson = Json.createObjectBuilder()
                    .add(JSON_IMAGE, dataManager.getBackgroundURI())
                    .build();
            // Add background image URI to background JSONbuiler
            backgroundBuilder.add(BACKGROUND_VALUE, backgroundValueJson);
        }
        // Build background JSON and load it into file JSON
        JsonObject backgroundJson = backgroundBuilder.build();
        dataJson.add(JSON_BACKGROUND, backgroundJson);
        
        // Make ArrayBuilder for MetroLines
        JsonArrayBuilder lineArray = Json.createArrayBuilder();
        // Retrieve lines from metMapData
        ArrayList<MetroLine> lines = dataManager.getLines();
        for (MetroLine line : lines) {
            // Set up builder for each MetroLine
            JsonObjectBuilder lineBuilder = Json.createObjectBuilder();
            lineBuilder.add(JSON_NAME, line.getName());
            JsonObject lineColor = makeColorJsonObject(line.getColor());
            lineBuilder.add(JSON_COLOR, lineColor)
                    .add(JSON_STROKE_WIDTH, line.getStrokeWidth())
                    .add(JSON_CIRCULAR, line.isCircular())
                    .add(JSON_START_X, line.getStartX())
                    .add(JSON_START_Y, line.getStartY())
                    .add(JSON_END_X, line.getEndX())
                    .add(JSON_END_Y, line.getEndY());
            MetroLabel label = line.getStartLabel();
            JsonObject lineStartLabel = Json.createObjectBuilder()
                    .add(JSON_FONT_FAMILY, label.getFamily())
                    .add(JSON_FONT_SIZE, label.getFontSize())
                    .add(JSON_FONT_BOLD, label.isBold())
                    .add(JSON_FONT_ITALIC, label.isItalic())
                    .add(JSON_FONT_COLOR, makeColorJsonObject(label.getTextFill()))
                    .build();
            lineBuilder.add(JSON_START_LABEL, lineStartLabel);
            label = line.getEndLabel();
            JsonObject lineEndLabel = Json.createObjectBuilder()
                    .add(JSON_FONT_FAMILY, label.getFamily())
                    .add(JSON_FONT_SIZE, label.getFontSize())
                    .add(JSON_FONT_BOLD, label.isBold())
                    .add(JSON_FONT_ITALIC, label.isItalic())
                    .add(JSON_FONT_COLOR, makeColorJsonObject(label.getTextFill()))
                    .build();
            lineBuilder.add(JSON_END_LABEL, lineEndLabel);
            JsonArrayBuilder lineStations = Json.createArrayBuilder();
            for (MetroStation station : line.getStations()) {
                lineStations.add(station.getName());
            }
            lineBuilder.add(JSON_STATION_NAMES, lineStations.build());
            lineArray.add(lineBuilder.build());
        }
        dataJson.add(JSON_LINES, lineArray.build());
        
        // Make ArrayBuilder for MetroStations
        JsonArrayBuilder stationArray = Json.createArrayBuilder();
        // Retrieve MetroStations from metMapData
        ArrayList<MetroStation> stations = dataManager.getStations();
        for (MetroStation station : stations) {
            JsonObjectBuilder stationBuilder = Json.createObjectBuilder();
            stationBuilder.add(JSON_NAME, station.getName());
            JsonObject stationColor = makeColorJsonObject(station.getColor());
            stationBuilder.add(JSON_COLOR, stationColor)
                    .add(JSON_X, station.getCenterX())
                    .add(JSON_Y, station.getCenterY())
                    .add(JSON_RADIUS, station.getRadius())
                    .add(JSON_LABELORIENT, station.getLabelOrient())
                    .add(JSON_ROTATED, station.rotated());
            MetroLabel label = station.getLabel();
            stationBuilder.add(JSON_FONT_FAMILY, label.getFamily())
                    .add(JSON_FONT_SIZE, label.getFontSize())
                    .add(JSON_FONT_BOLD, label.isBold())
                    .add(JSON_FONT_ITALIC, label.isItalic())
                    .add(JSON_FONT_COLOR, makeColorJsonObject(label.getTextFill()));
            stationArray.add(stationBuilder.build());
        }
        dataJson.add(JSON_STATIONS, stationArray.build());
        
        // Make ArrayBuilder for remaining map elements
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        // Retrieve elements from metMapData
        ObservableList<Node> elements = dataManager.getElements();
        for (Node element : elements) {
            if (element instanceof Draggable) {
                // Get type, x-, and y-coordinates using Draggable interface methods
                Draggable obj = (Draggable) element;
                if (obj instanceof MetroImage) {
                    // Set up builder
                    JsonObjectBuilder imageBuilder = Json.createObjectBuilder();
                    
                    double x = obj.getX();
                    double y = obj.getY();
                    imageBuilder.add(JSON_TYPE, JSON_IMAGE).add(JSON_X, x).add(JSON_Y, y);

                    MetroImage image = (MetroImage) obj;
                    imageBuilder.add(JSON_IMAGE, image.getImageURI());
                    
                    JsonObject imageJson = imageBuilder.build();
                    arrayBuilder.add(imageJson);
                } else if (obj instanceof MetroLabel) {
                    // Set up builder
                    MetroLabel label = (MetroLabel) obj;
                    if (!label.hasParent()) {
                        JsonObjectBuilder labelBuilder = Json.createObjectBuilder();

                        double x = obj.getX();
                        double y = obj.getY();
                        labelBuilder.add(JSON_TYPE, JSON_LABEL).add(JSON_X, x).add(JSON_Y, y);

                        labelBuilder.add(JSON_TEXT, label.getText())
                                .add(JSON_FONT_FAMILY, label.getFamily())
                                .add(JSON_FONT_SIZE, label.getFontSize())
                                .add(JSON_FONT_BOLD, label.isBold())
                                .add(JSON_FONT_ITALIC, label.isItalic())
                                .add(JSON_FONT_COLOR, makeColorJsonObject(label.getTextFill()));
                        JsonObject labelJson = labelBuilder.build();
                        arrayBuilder.add(labelJson);
                    }
                }
            }
        }
        // Build element JSON array and place in file JSON
        JsonArray elementArray = arrayBuilder.build();
        dataJson.add(JSON_ELEMENTS, elementArray);
        
        // Build file JSON
        JsonObject savedDataJson = dataJson.build();
        
        Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(savedDataJson);
	jsonWriter.close();

	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(savedDataJson);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
        
        pathPieces = this.mapName.split("\\\\");
        this.mapName = pathPieces[pathPieces.length - 1];
    }
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        metMapData dataManager = (metMapData) data;
        dataManager.resetData();
        
        JsonObject json = loadJSONFile(filePath);
        
        dataManager.setCanvasHeight(getDataAsDouble(json, JSON_CANVAS_HEIGHT));
        dataManager.setCanvasWidth(getDataAsDouble(json, JSON_CANVAS_WIDTH));
        
        JsonObject backgroundJson = json.getJsonObject(JSON_BACKGROUND);
        String backgroundType = backgroundJson.getString(BACKGROUND_TYPE);
        if (backgroundType.equals(BACKGROUND_COLOR)) {
            Color backgroundColor = loadColor(backgroundJson, BACKGROUND_VALUE);
            dataManager.setBackground(backgroundColor);
        } else if (backgroundType.equals(BACKGROUND_IMAGE)) {
            JsonObject backgroundValue = backgroundJson.getJsonObject(BACKGROUND_VALUE);
            dataManager.setBackgroundURI(backgroundValue.getString(JSON_IMAGE));
            ImagePattern backgroundImage = new ImagePattern(
                    loadImage(backgroundJson, BACKGROUND_VALUE));
            dataManager.setBackground(backgroundImage);
        }
        
        JsonArray jsonStations = json.getJsonArray(JSON_STATIONS);
        MetroStation station; int size = jsonStations.size();
        ArrayList<MetroStation> stations = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            station = loadStation(jsonStations.getJsonObject(i));
            stations.add(station);
        }
        
        JsonArray jsonLines = json.getJsonArray(JSON_LINES);
        MetroLine line; size = jsonLines.size();
        ArrayList<MetroLine> lines = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            line = loadLine(jsonLines.getJsonObject(i), stations);
            lines.add(line);
        }
        
        size = lines.size();
        for (int i = 0; i < size; i++) {
            line = lines.get(i);
            dataManager.addShape(line.getStartHighlight());
            dataManager.addShape(line.getEndHighlight());
            dataManager.addShape(line);
            dataManager.addShape(line.getStartLabel());
            dataManager.addShape(line.getEndLabel());
        }
        size = stations.size();
        for (int i = 0; i < size; i++) {
            station = stations.get(i);
            dataManager.addShape(station);
            dataManager.addShape(station.getLabel());            
        }
        
        JsonArray jsonElements = json.getJsonArray(JSON_ELEMENTS);
        MetroImage image; MetroLabel label; JsonObject element; size = jsonElements.size();
        for (int i = 0; i < size; i++) {
            element = jsonElements.getJsonObject(i);
            if (element.getString(JSON_TYPE).equals(JSON_IMAGE)) {
                image = loadMetroImage(element);
                dataManager.addShape(image);
            } else if (element.getString(JSON_TYPE).equals(JSON_LABEL)) {
                label = loadMetroLabel(element);
                dataManager.addShape(label);
            }
        }
        String[] pathPieces = filePath.split("\\\\");
        this.mapName = pathPieces[pathPieces.length - 1];
    }
    public void makeExportDirectory(String dirPath) {
        File exportDir = new File(dirPath);
        if (!exportDir.exists()) {
            exportDir.mkdir();
        }
    }
    public File exportMapImage() {
	metMapWorkspace workspace = (metMapWorkspace) this.app.getWorkspaceComponent();
	Pane canvas = workspace.getCanvas();
        SnapshotParameters param = new SnapshotParameters();
        param.setViewport(new Rectangle2D(0, 0, canvas.getWidth(), canvas.getHeight()));
	WritableImage image = canvas.snapshot(param, null);
        File file = new File(PATH_EXPORT + this.mapName + "/" + this.mapName + " Metro.png");
	try {
	    ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
	} catch (IOException ioe) {
	    ioe.printStackTrace();
	} catch (Exception e) {
            e.printStackTrace();
        } finally {
            return file;
        }
    }
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        // Get data manager for Metro Map Maker
        metMapData dataManager = (metMapData) data;
        // Make JSON builder for whole file
        JsonObjectBuilder dataJson = Json.createObjectBuilder();
        
        dataJson.add(JSON_NAME, this.mapName);
        
        // Make ArrayBuilder for MetroLines
        JsonArrayBuilder lineArray = Json.createArrayBuilder();
        // Retrieve lines from metMapData
        ArrayList<MetroLine> lines = dataManager.getLines();
        for (MetroLine line : lines) {
            // Set up builder for each MetroLine
            JsonObjectBuilder lineBuilder = Json.createObjectBuilder();
            lineBuilder.add(JSON_NAME, line.getName())
                    .add(JSON_CIRCULAR, line.isCircular());
            JsonObject lineColor = makeColorJsonObject(line.getColor());
            lineBuilder.add(JSON_COLOR, lineColor);
            JsonArrayBuilder lineStations = Json.createArrayBuilder();
            for (MetroStation station : line.getStations()) {
                lineStations.add(station.getName());
            }
            lineBuilder.add(JSON_STATION_NAMES, lineStations.build());
            lineArray.add(lineBuilder.build());
        }
        dataJson.add(JSON_LINES, lineArray.build());
        
        // Make ArrayBuilder for MetroStations
        JsonArrayBuilder stationArray = Json.createArrayBuilder();
        // Retrieve MetroStations from metMapData
        ArrayList<MetroStation> stations = dataManager.getStations();
        for (MetroStation station : stations) {
            JsonObjectBuilder stationBuilder = Json.createObjectBuilder();
            stationBuilder.add(JSON_NAME, station.getName())
                    .add(JSON_X, station.getCenterX())
                    .add(JSON_Y, station.getCenterY());
            stationArray.add(stationBuilder.build());
        }
        dataJson.add(JSON_STATIONS, stationArray.build());
        
        // Build file JSON
        JsonObject savedDataJson = dataJson.build();
        
        Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(savedDataJson);
	jsonWriter.close();
        
        File exportDir = new File(filePath);
        if (!exportDir.exists()) {
            exportDir.mkdir();
        }
        File exportFile = new File(filePath + "/" + this.mapName + " Metro.json");
	OutputStream os = new FileOutputStream(exportFile);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(savedDataJson);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(exportFile);
	pw.write(prettyPrinted);
	pw.close();
        
        exportMapImage();
    }
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
    
    }
}
