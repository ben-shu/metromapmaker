# MetroMapMaker
This was the final project assigned for the Fall 2017 section of CSE 219 at Stony Brook University.
It is a Java desktop application designed for creating city subway maps, which can then be saved 
and exported as PNG images.

IMPORTANT: The DesktopJavaFramework, PropertiesManager, and JTPS projects used for this application 
           were written and provided by Professor Richard McKenna, with corresponding author tags. 
