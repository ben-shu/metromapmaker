package djf.ui;

import djf.AppTemplate;
import properties_manager.PropertiesManager;

import javafx.stage.Stage;
import javafx.scene.Scene;

import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import static djf.settings.AppPropertyType.*;
import static djf.settings.AppStartupConstants.*;

import java.io.File;
import java.io.IOException;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;

/**
 * Singleton class designed to provide Metro Map Maker's welcome dialog.
 * 
 * @author Benjamin Shu
 */
public class AppWelcomeDialogSingleton extends Stage {
    private static final String FILE_NAME_PROMPT = "Enter file name:";
    private static final String FILE_EXISTS_TITLE = "File Already Exists";
    private static final String FILE_EXISTS_MESSAGE = "A file with that name already exists.";
    
    private static AppWelcomeDialogSingleton singleton;
    private static AppTemplate app;
    private static PropertiesManager props;
    private static AppTextInputDialogSingleton textInput;
    private static Scene scene;
    private static VBox layout;
    private static HBox inset;
    private static VBox recent;
    private static Label display;
    private static Button newMap;
    private static ImageView logo;
    
    private AppWelcomeDialogSingleton() { }
    public static AppWelcomeDialogSingleton getSingleton() {
        if (singleton == null) {
            singleton = new AppWelcomeDialogSingleton();
        }
        return singleton;
    }
    public void init(AppTemplate initApp) {
        // App that this dialog is part of
        app = initApp;
        
        // Get properties manager
        props = PropertiesManager.getPropertiesManager();
        
        // Set up text input dialog in case user wants to make a new map
        textInput = AppTextInputDialogSingleton.getSingleton();
        textInput.init(this);
        
        // Set up layout
        layout = new VBox();
        
        // Initialize label for window
        display = new Label(WELCOME_DISPLAY);
        display.setFont(Font.font("System Regular", FontWeight.BOLD, FontPosture.REGULAR, 18));
        
        // Set up inset for display
        inset = new HBox();
        
        // Set up list of recently edited maps
        recent = new VBox();
        
        // Set up logo
        logo = new ImageView(new Image(FILE_PROTOCOL + PATH_IMAGES + props.getProperty(APP_LOGO)));
        logo.setFitHeight(50);
        logo.setFitWidth(50);
        
        // Button for creating new maps.
        newMap = new Button(NEWMAP_BUTTON_LABEL);
        newMap.setOnAction(e -> {
            textInput.showDialog(FILE_NAME_PROMPT);
            String text = textInput.getText();
            if (!text.equals("")) {
                try {
                    File newMapFile = new File(PATH_WORK + text);
                    if (newMapFile.exists()) {
                        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                        dialog.show(FILE_EXISTS_TITLE, FILE_EXISTS_MESSAGE);
                    } else {
                        app.getFileComponent().saveData(app.getDataComponent(), newMapFile.getPath());
                        app.getGUI().getFileController().updateAfterLoad(newMapFile);
                        app.getFileComponent().loadData(app.getDataComponent(), newMapFile.getPath());
                        app.getFileComponent().makeExportDirectory(PATH_EXPORT + text);
                        app.getWorkspaceComponent().activateWorkspace(app.getGUI().getAppPane());
                        this.close();
                    }
                } catch (IOException i) {
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show(props.getProperty(SAVE_ERROR_TITLE),
                            props.getProperty(SAVE_ERROR_MESSAGE));
                }
            }
        });
        newMap.setMaxSize(150, 150);
        newMap.setMinSize(150, 150);
        
        inset.getChildren().addAll(recent, newMap);
        recent.setAlignment(Pos.CENTER);
        newMap.setAlignment(Pos.CENTER);
        inset.setAlignment(Pos.CENTER);
        inset.setSpacing(20);
        
        layout.getChildren().addAll(display, inset, logo);
        display.setAlignment(Pos.CENTER);
        layout.setAlignment(Pos.CENTER);
        layout.setSpacing(20);
        
        scene = new Scene(layout, 400, 400);
        this.setScene(scene);
        this.setTitle(WELCOME_TITLE);
    }
    public void addRecentHyperink(File file) {
        // Create new hyperlink using file name
        Hyperlink recentFile = new Hyperlink(file.getName());
        recentFile.setOnAction(e -> {
            try {
                app.getFileComponent().loadData(app.getDataComponent(), file.getAbsolutePath());
                app.getWorkspaceComponent().activateWorkspace(app.getGUI().getAppPane());
                app.getWorkspaceComponent().reloadWorkspace(app.getDataComponent());
                app.getGUI().getFileController().updateAfterLoad(file);
                this.close();
            } catch (IOException i) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(LOAD_ERROR_TITLE), props.getProperty(LOAD_ERROR_MESSAGE));
            }
        });
        recent.getChildren().add(recentFile);
    }
    public void showDialog() {
        showAndWait();
    }
}
