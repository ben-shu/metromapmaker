package djf.ui;

import javafx.stage.Stage;
import javafx.stage.Modality;
import javafx.scene.Scene;

import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import static djf.settings.AppStartupConstants.TEXT_INPUT_TITLE;
import static djf.settings.AppStartupConstants.CANCEL_BUTTON_LABEL;
import static djf.settings.AppStartupConstants.ENTER_BUTTON_LABEL;

/**
 * Singleton class designed to provide a pop-up UI window for user text entry.
 * 
 * @author Benjamin Shu
 */
public class AppTextInputDialogSingleton extends Stage {
    private static AppTextInputDialogSingleton singleton;
    private static Scene scene;
    private static VBox layout;
    private static Label display;
    private static TextArea textEntry;
    private static String text;
    private static HBox buttons;
    private static Button enter;
    private static Button cancel;
    
    private AppTextInputDialogSingleton() { }
    public static AppTextInputDialogSingleton getSingleton() {
        if (singleton == null) {
            singleton = new AppTextInputDialogSingleton();
        }
        return singleton;
    }
    public void init(Stage owner) {
        // Make this a modal window, set the workspace stage as the owner
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);
        
        // Label window, so the user knows what to do
        display = new Label(); // Get text from AppStartupConstants
        
        // Initialize text area
        textEntry = new TextArea("");
        textEntry.setMinHeight(25); textEntry.setMaxHeight(25);
        textEntry.setMinWidth(150); textEntry.setMaxWidth(150);
        
        // Set up buttons in a neat little row
        buttons = new HBox();
        enter = new Button(ENTER_BUTTON_LABEL);
        enter.setOnAction(e -> {
            text = textEntry.getText();
            textEntry.setText("");
            AppTextInputDialogSingleton.this.close();
        });
        cancel = new Button(CANCEL_BUTTON_LABEL);
        cancel.setOnAction(e -> {
            textEntry.setText("");
            text = "";
            AppTextInputDialogSingleton.this.close();
        });
        buttons.getChildren().addAll(enter, cancel);
        buttons.setAlignment(Pos.CENTER);
        
        // If user closes this window, cancel file name prompt
        setOnCloseRequest(e -> {
            textEntry.setText("");
            text = "";
        });
        
        // Set up layout
        layout = new VBox();
        layout.getChildren().addAll(display, textEntry, buttons);
        layout.setAlignment(Pos.CENTER);
        layout.setSpacing(20);
        scene = new Scene(layout, 200, 200);
        this.setScene(scene);
        this.setTitle(TEXT_INPUT_TITLE);
    }
    public String getText() {
        return this.text;
    }
    public void showDialog(String message) {
        display.setText(message);
        showAndWait();
    }
}